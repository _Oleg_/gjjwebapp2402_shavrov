package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.contacts.Contact;
import com.getjavajob.training.web1610.shavrovo.contacts.Messenger;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.util.Set;

/**
 * Created by saul on 4/26/2017.
 */
public class MessengerDAO extends AbstractContactsDAO {
    @Override
    public Messenger createRelation(Account entity, Contact boundedEntity) throws DAOException {
        return (Messenger) genericCreateRelation(entity, boundedEntity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<Messenger> getAllBounded(Account entity) throws DAOException {
        return (Set<Messenger>) genericGetAllBounded(entity, Messenger.class);
    }

    @Override
    public boolean deleteRelation(Account entity, Contact boundedEntity) throws DAOException {
        return genericDeleteRelation(entity, boundedEntity);
    }

    @Override
    public Messenger getById(int id) throws DAOException {
        return (Messenger) genericGetById(id, Messenger.class);

    }

    @Override
    public Messenger create(Contact entity) throws DAOException {
        return (Messenger) super.create(entity);
    }

    @Override
    public Messenger update(Contact entity) throws DAOException {
        return (Messenger) updateGeneric(entity, Messenger.class);
    }

    @Override
    public boolean delete(Contact entity) throws DAOException {
        return genericDelete(entity, Messenger.class);
    }

}
