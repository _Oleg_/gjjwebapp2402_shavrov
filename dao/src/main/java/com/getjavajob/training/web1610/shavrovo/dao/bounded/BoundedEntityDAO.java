package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.util.Set;

/**
 * Created by saul on 20.04.2017.
 */
public interface BoundedEntityDAO<E, B> {
    B createRelation(E entity, B boundedEntity) throws DAOException;

    Set<? extends B> getAllBounded(E entity) throws DAOException;

    boolean deleteRelation(E entity, B boundedEntity) throws DAOException;
}
