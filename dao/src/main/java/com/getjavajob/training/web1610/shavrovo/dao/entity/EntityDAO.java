package com.getjavajob.training.web1610.shavrovo.dao.entity;

import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.util.Set;

/**
 * Created by saul on 20.04.2017.
 */
public interface EntityDAO<E> {
    Set<E> getAll() throws DAOException;

    E create(E entity) throws DAOException;

    E getById(int id) throws DAOException;

    E update(E entity) throws DAOException;

    boolean delete(E entity) throws DAOException;

    E getEntityByUniqueKey(String key) throws DAOException;

    Set<E> getAllByKeyValuePair(String key, String value) throws DAOException;
}
