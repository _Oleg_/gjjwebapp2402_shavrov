package com.getjavajob.training.web1610.shavrovo;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.DAO_PROPERTIES;

/**
 * Created by saul on 22.03.2017.
 */
public class ConnectionPool {
    private static final String PROPS_PATH = DAO_PROPERTIES;
    private static final ConnectionPool connectionPool = new ConnectionPool(PROPS_PATH);
    private static BlockingQueue<Connection> pool;
    private static ThreadLocal<Connection> threadLocalScope;
    private static DataSource ds;


    private ConnectionPool(String propertiesPath) {
        Properties properties = new Properties();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream in = classLoader.getResourceAsStream(propertiesPath)) {
            if (in != null) {
                properties.load(in);
                int poolSize = Integer.valueOf(properties.getProperty("connection_pool_size"));
                pool = new LinkedBlockingQueue<>(poolSize);
                for (int i = 0; i < poolSize; i++) {
                    pool.offer(setUpConnection(properties));
                }
            } else {
                initializeDSByContext();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        threadLocalScope = new ThreadLocal<>();
    }

    public static ConnectionPool getInstance() {
        return connectionPool;
    }

    private void initializeDSByContext() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            ds = (DataSource) envContext.lookup("jdbc/webapp");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private Connection setUpConnection(Properties properties) {
        Connection connection = null;
        try {
            String driver = properties.getProperty("driver");
            Class.forName(driver);
            String url = properties.getProperty("url");
            connection = DriverManager.getConnection(url, properties);
            connection = setConnectionProperties(connection);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private Connection setConnectionProperties(Connection connection) throws SQLException {
        connection.setAutoCommit(false);
        connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return connection;
    }

    public Connection acquireConnection() {
        Connection connection = threadLocalScope.get();
        if (connection == null) {
            if (ds == null) {
                try {
                    connection = pool.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    connection = setConnectionProperties(ds.getConnection());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            threadLocalScope.set(connection);
        }
        return connection;
    }

    public void releaseConnection() {
        Connection connection = threadLocalScope.get();
        if (ds == null) {
            pool.offer(connection);
        } else {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        threadLocalScope.remove();
    }

    public void commit() {
        Connection connection = threadLocalScope.get();
        try {
            connection.commit();
        } catch (SQLException e) {
            System.err.println("Error due to transaction commit.");
            e.printStackTrace();
        }
    }

    public void rollback() {
        Connection connection = threadLocalScope.get();
        try {
            connection.rollback();
        } catch (SQLException e) {
            System.err.println("Transaction is being rolling back.");
            e.printStackTrace();
        }
    }
}
