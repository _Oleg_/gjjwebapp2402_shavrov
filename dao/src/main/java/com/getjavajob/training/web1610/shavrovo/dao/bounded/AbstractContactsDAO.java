package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.contacts.*;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.*;

/**
 * Created by saul on 4/26/2017.
 */
public abstract class AbstractContactsDAO implements EntityDAO<Contact>, BoundedEntityDAO<Account, Contact> {
    private ConnectionPool connectionPool;

    public AbstractContactsDAO() {
        connectionPool = ConnectionPool.getInstance();
    }


    @Override
    public Set<Contact> getAll() throws DAOException {
        throw new UnsupportedOperationException("getAll() is not available for all bounded");
    }

    @Override
    public Contact create(Contact entity) throws DAOException {
        String genericQuery = "INSERT INTO %s (login, contact_type) VALUES (?, ?)";
        String insertQuery;
        if (entity.getContactType() instanceof PhoneType) {
            insertQuery = String.format(genericQuery, PHONES_TABLE);
        } else {
            insertQuery = String.format(genericQuery, MESSENGERS_TABLE);
        }
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, entity.getLogin());
            ps.setString(2, entity.getContactType().contactType());
            ps.executeUpdate();
            ResultSet generatedKeys = ps.getGeneratedKeys();
            if (generatedKeys.next()) {
                entity.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return entity;
    }

    @Override
    public abstract Contact getById(int id) throws DAOException;

    protected Contact genericGetById(int id, Class type) throws DAOException {
        Contact contact = null;
        String genericQuery = "SELECT * FROM %s WHERE id = ?";
        String query = null;
        if (type == Phone.class) {
            query = String.format(genericQuery, PHONES_TABLE);
        } else if (type == Messenger.class) {
            query = String.format(genericQuery, MESSENGERS_TABLE);
        }
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                String login = resultSet.getString("login");
                ContactType contactType;
                if (type == Phone.class) {
                    contactType = PhoneType.valueOf(resultSet.getString("contact_type"));
                    contact = new Phone(login, contactType);
                    contact.setId(id);
                } else if (type == Messenger.class) {
                    contactType = MessengerType.valueOf(resultSet.getString("contact_type"));
                    contact = new Messenger(login, contactType);
                    contact.setId(id);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return contact;
    }


    @Override
    public abstract Contact update(Contact entity) throws DAOException;

    protected Contact updateGeneric(Contact entity, Class type) throws DAOException {
        Contact updatedEntity = null;
        String genericCheckQuery = "SELECT * FROM %s WHERE id = ?";
        String genericUpdateQuery = "UPDATE %s SET login = ?, contact_type = ? WHERE id = ?";
        String checkQuery = null;
        String updateQuery = null;
        if (type == Phone.class) {
            checkQuery = String.format(genericCheckQuery, PHONES_TABLE);
            updateQuery = String.format(genericUpdateQuery, PHONES_TABLE);
        } else if (type == Messenger.class) {
            checkQuery = String.format(genericCheckQuery, MESSENGERS_TABLE);
            updateQuery = String.format(genericUpdateQuery, MESSENGERS_TABLE);
        }
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (
                PreparedStatement checkPS = connection.prepareStatement(checkQuery);
                PreparedStatement updatePS = connection.prepareStatement(updateQuery);
        ) {
            checkPS.setInt(1, entity.getId());
            ResultSet resultSet = checkPS.executeQuery();
            updatedEntity = updateContact(entity, resultSet, updatePS);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return updatedEntity;
    }

    @Override
    public abstract boolean delete(Contact entity) throws DAOException;

    protected boolean genericDelete(Contact entity, Class type) throws DAOException {
        String deleteQuery = null;
        if (type == Phone.class) {
            deleteQuery = "DELETE FROM " + PHONES_TABLE + " WHERE id = ?";
        } else if (type == Messenger.class) {
            deleteQuery = "DELETE FROM " + MESSENGERS_TABLE + " WHERE id = ?";
        }
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setInt(1, entity.getId());
            int updated = ps.executeUpdate();
            return updated != 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Contact getEntityByUniqueKey(String key) throws DAOException {
        throw new UnsupportedOperationException("Select by unique key is not supported for bounded");
    }

    @Override
    public Set<Contact> getAllByKeyValuePair(String key, String value) throws DAOException {
        throw new UnsupportedOperationException("Select by key-value pair is not supported for bounded");
    }

    protected Contact genericCreateRelation(Account entity, Contact contact) throws DAOException {
        String genericCreateQuery = "INSERT INTO %s (account_id, contact_id) VALUES (?, ?)";
        String createQuery = null;
        if (contact instanceof Phone) {
            createQuery = String.format(genericCreateQuery, ACCOUNT_PHONES_TABLE);
        } else if (contact instanceof Messenger) {
            createQuery = String.format(genericCreateQuery, ACCOUNT_MESSENGERS_TABLE);
        }
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setInt(1, entity.getId());
            ps.setInt(2, contact.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return contact;
    }

    protected Set<? extends Contact> genericGetAllBounded(Account entity, Class type) throws DAOException {
        Set<Contact> bounded = new HashSet<>();
        String genericSelectQuery = "SELECT * FROM %s WHERE account_id = ?";
        String selectQuery = null;
        if (type == Phone.class) {
            selectQuery = String.format(genericSelectQuery, ACCOUNT_PHONES_TABLE);
        } else if (type == Messenger.class) {
            selectQuery = String.format(genericSelectQuery, ACCOUNT_MESSENGERS_TABLE);
        }
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(selectQuery)) {
            ps.setInt(1, entity.getId());
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("contact_id");
                if (type == Phone.class) {
                    bounded.add(genericGetById(id, Phone.class));
                } else if (type == Messenger.class) {
                    bounded.add(genericGetById(id, Messenger.class));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bounded;
    }

    public boolean genericDeleteRelation(Account entity, Contact boundedEntity) throws DAOException {
        checkDefaultId(entity);
        String genericDeleteQuery = "DELETE FROM %s WHERE account_id = ? and contact_id = ?";
        String deleteQuery = null;
        if (boundedEntity instanceof Phone) {
            deleteQuery = String.format(genericDeleteQuery, ACCOUNT_PHONES_TABLE);
        } else if (boundedEntity instanceof Messenger) {
            deleteQuery = String.format(genericDeleteQuery, ACCOUNT_MESSENGERS_TABLE);
        }
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setInt(1, entity.getId());
            ps.setInt(2, boundedEntity.getId());
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Contact updateContact(Contact boundedEntity, ResultSet resultSet, PreparedStatement updatePrepStmt) throws SQLException {
        if (resultSet.next()) {
            updatePrepStmt.setString(1, boundedEntity.getLogin());
            updatePrepStmt.setString(2, boundedEntity.getContactType().contactType());
            updatePrepStmt.setInt(3, boundedEntity.getId());
            updatePrepStmt.executeUpdate();
            return boundedEntity;
        }
        return null;
    }

    protected void checkDefaultId(Account account) throws DAOException {
        if (account.getId() < 0) {
            throw new DAOException("Id for account is not set. The default id = " + Integer.toHexString(account.getId()) + " cannot be used.", new IllegalArgumentException());
        }
    }

}
