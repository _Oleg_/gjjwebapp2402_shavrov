package com.getjavajob.training.web1610.shavrovo;

import java.sql.*;

/**
 * Created by saul on 27.02.2017.
 */
public class DBInitializer {
    public static final String DAO_PROPERTIES = "dao.properties";
    public static final String COMMON_PROPERTIES = "common.properties";
    public static final String ACCOUNTS_TABLE = "Accounts";
    public static final String PHONES_TABLE = "Phones";
    public static final String MESSENGERS_TABLE = "Messengers";
    public static final String ACCOUNT_PHONES_TABLE = "Account_phones";
    public static final String ACCOUNT_MESSENGERS_TABLE = "Account_messengers";
    public static final String RELATIONSHIPS_TABLE = "Relationships";
    public static final String GROUPS_TABLE = "Groups";
    public static final String GROUP_MEMBERS_TABLE = "Group_members";
    public static final String PERMISSIONS_TABLE = "Permissions";
    public static final String ACCOUNT_PERMISSIONS_TABLE = "Account_permissions";


    public static void createTables(String pathToTablesInitScript) {
        String filePath = ClassLoader.getSystemClassLoader().getResource(pathToTablesInitScript).getPath();
        String osAppropriatePath = System.getProperty("os.name").contains("indow") ? filePath.substring(1) : filePath;
        String tableInitQuery = String.format("RUNSCRIPT FROM '%s';", osAppropriatePath);
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(tableInitQuery);
            ConnectionPool.getInstance().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().releaseConnection();
        }
    }
}
