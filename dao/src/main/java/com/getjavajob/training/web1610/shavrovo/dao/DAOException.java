package com.getjavajob.training.web1610.shavrovo.dao;

/**
 * Created by saul on 29.03.2017.
 */
public class DAOException extends Exception {
    public DAOException(Throwable cause) {super(cause);}
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
