package com.getjavajob.training.web1610.shavrovo.dao.relations;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.RELATIONSHIPS_TABLE;

/**
 * Created by saul on 5/4/2017.
 */
public class AbstractRelationDAO implements RelationshipDAO<Account> {
    protected EntityDAO<Account> accountDAO;
    private ConnectionPool connectionPool;

    public AbstractRelationDAO() {
        accountDAO = new AccountDAO();
        connectionPool = ConnectionPool.getInstance();
    }

    @Override
    public void createRelation(Account from, Account to, RelationStatus status) throws DAOException {
        String query = "INSERT INTO " + RELATIONSHIPS_TABLE + " (from_id, to_id, status) VALUES (?, ?, ?)";
        singleRelationUpdate(from, to, status, query);
    }

    @Override
    public void deleteRelation(Account from, Account to, RelationStatus status) throws DAOException {
        String query = "DELETE FROM " + RELATIONSHIPS_TABLE + " WHERE from_id = ? and to_id = ? and status = ?";
        singleRelationUpdate(from, to, status, query);
    }

    @Override
    public Set<Account> getAllRelatedFrom(Account from, RelationStatus status) throws DAOException {
        return getAllRelated(from, status, true);
    }

    @Override
    public Set<Account> getAllRelatedTo(Account to, RelationStatus status) throws DAOException {
        return getAllRelated(to, status, false);
    }

    private void singleRelationUpdate(Account from, Account to, RelationStatus status, String query) throws DAOException {
        Connection connection = connectionPool.acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, from.getId());
            ps.setInt(2, to.getId());
            ps.setString(3, status.name());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    private Set<Account> getAllRelated(Account account, RelationStatus status, boolean from) throws DAOException {
        String query = null;
        if (from) {
            query = "SELECT to_id FROM " + RELATIONSHIPS_TABLE + " WHERE from_id = ? and status = ?";
        } else {
            query = "SELECT from_id FROM " + RELATIONSHIPS_TABLE + " WHERE to_id = ? and status = ?";
        }
        Connection connection = connectionPool.acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, account.getId());
            ps.setString(2, status.name());
            ResultSet resultSet = ps.executeQuery();
            connectionPool.commit();

            Set<Account> accounts = new HashSet<>();
            while (resultSet.next()) {
                int id = from ? resultSet.getInt("to_id") : resultSet.getInt("from_id");
                Account relatedAccount = accountDAO.getById(id);
                accounts.add(relatedAccount);
            }
            return accounts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void createAllRelatedFrom(Account from, RelationStatus status) throws DAOException {
        String query = "INSERT INTO " + RELATIONSHIPS_TABLE + " (from_id, to_id, status) VALUES (?, ?, ?)";
        Set<Account> accountsToSave = new HashSet<>();
        switch (status) {
            case FRIEND:
                accountsToSave = from.getFriends();
                break;
            case SUBSCRIBER:
                accountsToSave = from.getSubscriptions();
                break;
            case FRIEND_PENDING:
                accountsToSave = from.getMyFriendsPending();
                break;
        }
        saveFromToRelation(from, status, query, accountsToSave, false);
    }

    @Override
    public void createAllRelatedTo(Account to, RelationStatus status) throws DAOException {
        String query = "INSERT INTO " + RELATIONSHIPS_TABLE + " (from_id, to_id, status) VALUES (?, ?, ?)";
        Set<Account> accountsToSave = new HashSet<>();
        switch (status) {
            case FRIEND:
                accountsToSave = to.getFriends();
                break;
            case SUBSCRIBER:
                accountsToSave = to.getSubscribers();
                break;
            case FRIEND_PENDING:
                accountsToSave = to.getTheirFriendsPending();
                break;
        }
        saveFromToRelation(to, status, query, accountsToSave, true);
    }

    private void saveFromToRelation(Account account, RelationStatus status, String query, Set<Account> accountsToSave, boolean reverse) throws DAOException {
        Connection connection = connectionPool.acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            for (Account accountToSave : accountsToSave) {
                if (!reverse) {
                    ps.setInt(1, account.getId());
                    ps.setInt(2, accountToSave.getId());
                } else {
                    ps.setInt(1, accountToSave.getId());
                    ps.setInt(2, account.getId());
                }
                ps.setString(3, status.name());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }
}
