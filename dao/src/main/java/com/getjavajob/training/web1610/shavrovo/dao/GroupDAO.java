package com.getjavajob.training.web1610.shavrovo.dao;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.Group;
import com.getjavajob.training.web1610.shavrovo.GroupBuilder;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.BoundedEntityDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.GROUPS_TABLE;
import static com.getjavajob.training.web1610.shavrovo.DBInitializer.GROUP_MEMBERS_TABLE;

/**
 * Created by saul on 27.02.2017.
 */
public class GroupDAO implements EntityDAO<Group>, BoundedEntityDAO<Account, Group> {
    private EntityDAO<Account> accountDAO;

    public GroupDAO() {
        accountDAO = new AccountDAO();
    }

    @Override
    public Set<Group> getAll() {
        Set<Group> allGroups = new HashSet<>();
        String selectAllGroups = "SELECT * FROM " + GROUPS_TABLE;
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(selectAllGroups);
            while (rs.next()) {
                String groupName = rs.getString("name");
                GroupBuilder builder = new GroupBuilder(groupName);
                builder.id(rs.getInt("id"));

                Group group = builder.createGroup();
                getGroupMembers(group);
                allGroups.add(group);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allGroups;
    }

    @Override
    public Group create(Group group) {
        String scalarDataStore = "INSERT INTO " + GROUPS_TABLE + " (name) VALUES (?)";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(scalarDataStore, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, group.getName());
            ps.executeUpdate();
            ResultSet resultSet = ps.getGeneratedKeys();
            if (resultSet.next()) {
                group.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        setGroupMembers(group);
        return group;
    }

    @Override
    public Group getById(int id) {
        String query = "SELECT name FROM " + GROUPS_TABLE + " WHERE id = ?";
        GroupBuilder builder = null;
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                builder = new GroupBuilder(resultSet.getString("name"));
                builder.id(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (builder != null) {
            Group group = builder.createGroup();
            getGroupMembers(group);
            return group;
        } else {
            return null;
        }
    }

    @Override
    public Group update(Group group) {
        String query = "UPDATE " + GROUPS_TABLE + " " +
                "SET " +
                "name = ? " +
                "WHERE id = ?;";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, group.getName());
            ps.setInt(2, group.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        setGroupMembers(group);
        return group;
    }

    @Override
    public boolean delete(Group group) {
        String plainGroupDataDeletionQuery = "DELETE FROM " + GROUPS_TABLE + " WHERE id = " + group.getId();
        String groupMembersDeletion = "DELETE FROM " + GROUP_MEMBERS_TABLE + " WHERE group_id = " + group.getId();
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(groupMembersDeletion);
            statement.executeUpdate(plainGroupDataDeletionQuery);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Group getEntityByUniqueKey(String key) throws DAOException {
        // TODO: 4/26/2017 implement
        return null;
    }

    private void getGroupMembers(Group group) {
        Set<Account> admins = new HashSet<>();
        Set<Account> members = new HashSet<>();
        String membersQuery = "SELECT * FROM " + GROUP_MEMBERS_TABLE + " WHERE group_id = " + group.getId();
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet membersRS = statement.executeQuery(membersQuery);
            while (membersRS.next()) {
                Account account = accountDAO.getById(membersRS.getInt("account_id"));
                Group.MemberStatus status = Group.MemberStatus.valueOf(membersRS.getString("status"));
                switch (status) {
                    case ADMIN:
                        members.add(account);
                        admins.add(account);
                        break;
                    case MEMBER:
                        members.add(account);
                        break;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        group.setMembers(members);
        group.setAdmins(admins);
    }

    private void setGroupMembers(Group group) {
        String deleteOldData = "DELETE FROM " + GROUP_MEMBERS_TABLE + " " +
                "WHERE group_id = " + group.getId();
        String membersUpdateQuery = "INSERT INTO " + GROUP_MEMBERS_TABLE + " " +
                "(group_id, account_id, status) " +
                "VALUES " +
                "(?, ?, ?);";

        Connection con1 = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = con1.createStatement();
             PreparedStatement ps = con1.prepareStatement(membersUpdateQuery);
        ) {
            statement.executeUpdate(deleteOldData);
            for (Account member : group.getMembers()) {
                ps.setInt(1, group.getId());
                ps.setInt(2, member.getId());
                if (group.getAdmins().contains(member)) {
                    ps.setString(3, String.valueOf(Group.MemberStatus.ADMIN));
                } else {
                    ps.setString(3, String.valueOf(Group.MemberStatus.MEMBER));
                }
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set<Group> getAllByKeyValuePair(String key, String value) throws DAOException {
        return null;
    }

    @Override
    public Group createRelation(Account entity, Group boundedEntity) throws DAOException {
        return null;
    }

    @Override
    public Set<? extends Group> getAllBounded(Account entity) throws DAOException {
        return null;
    }

    @Override
    public boolean deleteRelation(Account entity, Group boundedEntity) throws DAOException {
        return false;
    }

}
