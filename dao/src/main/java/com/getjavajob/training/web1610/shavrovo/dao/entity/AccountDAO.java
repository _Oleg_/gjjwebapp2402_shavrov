package com.getjavajob.training.web1610.shavrovo.dao.entity;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.sql.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.ACCOUNTS_TABLE;

/**
 * Created by Oleg Shavrov
 * on 27.02.2017.
 */
public class AccountDAO implements EntityDAO<Account> {
    @Override
    public Set<Account> getAll() throws DAOException {
        Set<Account> accounts = new HashSet<>();
        String selectAllAccounts = "SELECT id FROM " + ACCOUNTS_TABLE;

        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(selectAllAccounts);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                Account account = getById(id);
                accounts.add(account);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getLocalizedMessage(), e.getCause());
        }
        return accounts;
    }

    @Override
    public Account create(Account account) throws DAOException {
        String query = "INSERT INTO %s " +
                "(regTimestamp, email, password, firstName, lastName, middleName, homeAddress, workAddress, avatarPath) " +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?);";
        String creationQuery = String.format(query, ACCOUNTS_TABLE);
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(creationQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setString(2, account.getEmail());
            ps.setString(3, account.getPassword());
            ps.setString(4, account.getFirstName());
            ps.setString(5, account.getLastName());
            ps.setString(6, account.getMiddleName());
            ps.setString(7, account.getHomeAddress());
            ps.setString(8, account.getWorkAddress());
            ps.setString(9, account.getAvatarPath());
            ps.executeUpdate();
            ResultSet generatedKeysRS = ps.getGeneratedKeys();
            if (generatedKeysRS.next()) {
                int id = generatedKeysRS.getInt(1);
                account.setId(id);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getLocalizedMessage(), e.getCause());
        }
        return account;
    }

    @Override
    public Account getById(int id) throws DAOException {
        Account account = null;
        String query = "SELECT * FROM " + ACCOUNTS_TABLE + " WHERE id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            AccountBuilder builder = new AccountBuilder();
            while (rs.next()) {
                builder.id(id);
                builder.regTimestamp(rs.getTimestamp("regTimestamp"));
                builder.email(rs.getString("email"));
                builder.password(rs.getString("password"));
                builder.firstName(rs.getString("firstName"));
                builder.lastName(rs.getString("lastName"));
                builder.middleName(rs.getString("middleName"));
                builder.homeAddress(rs.getString("homeAddress"));
                builder.workAddress(rs.getString("workAddress"));
                builder.avatarPath(rs.getString("avatarPath"));
                account = builder.createAccount();
            }
        } catch (SQLException e) {
            throw new DAOException(e.getLocalizedMessage(), e.getCause());
        }
        return account;
    }

    @Override
    public Account update(Account account) throws DAOException {
        String updateQuery = "UPDATE " + ACCOUNTS_TABLE + " SET " +
                "email = ?, " +
                "password = ?, " +
                "firstName = ?, " +
                "lastName = ?, " +
                "middleName = ?, " +
                "homeAddress = ?, " +
                "workAddress = ?, " +
                "avatarPath = ? " +
                "WHERE id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(updateQuery)) {
            ps.setString(1, account.getEmail());
            ps.setString(2, account.getPassword());
            ps.setString(3, account.getFirstName());
            ps.setString(4, account.getLastName());
            ps.setString(5, account.getMiddleName());
            ps.setString(6, account.getHomeAddress());
            ps.setString(7, account.getWorkAddress());
            ps.setString(8, account.getAvatarPath());
            ps.setInt(9, account.getId());
            ps.executeUpdate();
            ConnectionPool.getInstance().commit();
        } catch (SQLException e) {
            throw new DAOException(e.getLocalizedMessage(), e.getCause());
        }
        return account;
    }

    @Override
    public boolean delete(Account account) throws DAOException {
        String deleteQuery = "DELETE FROM " + ACCOUNTS_TABLE + " WHERE id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
            ps.setInt(1, account.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getLocalizedMessage(), e.getCause());
        }
        return true;
    }

    @Override
    public Account getEntityByUniqueKey(String key) throws DAOException {
        String query = "Select id from " + ACCOUNTS_TABLE + " where email = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, key);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                return getById(id);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getLocalizedMessage(), e);
        }
        return null;
    }

    @Override
    public Set<Account> getAllByKeyValuePair(String key, String value) throws DAOException {
        Set<Account> results = new HashSet<>();
        String query = getAllowedQuery(key);
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setString(1, "%" +value + "%");
            ResultSet resultSet = ps.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                results.add(getById(id));
            }
        } catch (SQLException e) {
            throw new DAOException(e.getLocalizedMessage(), e);
        }
        return results;
    }

    private String getAllowedQuery(String key) throws DAOException {
        Map<String, String> allowedQueries = new HashMap<>();
        allowedQueries.put("firstName", "Select id from " + ACCOUNTS_TABLE + " where firstName like ?");
        allowedQueries.put("lastName", "Select id from " + ACCOUNTS_TABLE + " where lastName like ?");
        allowedQueries.put("middleName", "Select id from " + ACCOUNTS_TABLE + " where middleName like ?");
        // TODO: 4/29/2017 obtain accounts by full name
        if (!allowedQueries.containsKey(key)) {
            throw new DAOException("The provided key " + key + " is not allowed for this query", new IllegalAccessException());
        }
        return allowedQueries.get(key);
    }
}
