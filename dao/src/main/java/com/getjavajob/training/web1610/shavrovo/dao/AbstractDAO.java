package com.getjavajob.training.web1610.shavrovo.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by saul on 5/23/2017.
 */
public class AbstractDAO {
    private DataSource ds;

    protected AbstractDAO() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            this.ds = (DataSource) envContext.lookup("jdbc/webapp");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    protected DataSource getDataSource() {
        return this.ds;
    }

    protected void commitIfNotNull(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected void closeIfNotNull(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
