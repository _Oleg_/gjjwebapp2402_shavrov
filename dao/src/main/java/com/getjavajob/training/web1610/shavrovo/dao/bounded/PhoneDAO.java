package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.contacts.Contact;
import com.getjavajob.training.web1610.shavrovo.contacts.Phone;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.util.Set;

/**
 * Created by saul on 4/26/2017.
 */
public class PhoneDAO extends AbstractContactsDAO {

    @Override
    public Phone createRelation(Account entity, Contact boundedEntity) throws DAOException {
        return (Phone) genericCreateRelation(entity, boundedEntity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<Phone> getAllBounded(Account account) throws DAOException {
        return (Set<Phone>) genericGetAllBounded(account, Phone.class);
    }

    @Override
    public boolean deleteRelation(Account entity, Contact boundedEntity) throws DAOException {
        return genericDeleteRelation(entity, boundedEntity);
    }

    @Override
    public Phone getById(int id) throws DAOException {
        return (Phone) genericGetById(id, Phone.class);
    }


    @Override
    public Phone create(Contact entity) throws DAOException {
        return (Phone) super.create(entity);
    }

    @Override
    public Phone update(Contact entity) throws DAOException {
        return (Phone) updateGeneric(entity, Phone.class);
    }

    @Override
    public boolean delete(Contact entity) throws DAOException {
        return genericDelete(entity, Phone.class);
    }

}
