package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.Permission;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.ACCOUNT_PERMISSIONS_TABLE;
import static com.getjavajob.training.web1610.shavrovo.DBInitializer.PERMISSIONS_TABLE;

/**
 * Created by saul on 5/13/2017.
 */
public class PermissionDAO implements BoundedEntityDAO<Account, Permission> {

    private Permission getById(int id) throws DAOException {
        String query = "SELECT * FROM " + PERMISSIONS_TABLE + " WHERE id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                return new Permission(id, name, description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Permission createRelation(Account entity, Permission boundedEntity) throws DAOException {
        String query = "INSERT INTO " + ACCOUNT_PERMISSIONS_TABLE + " (account_id, permission_id) VALUES (?, ?)";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, entity.getId());
            ps.setInt(2, boundedEntity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return boundedEntity;
    }

    @Override
    public Set<Permission> getAllBounded(Account entity) throws DAOException {
        Set<Permission> permissions = new HashSet<>();
        String query = "SELECT * FROM " + ACCOUNT_PERMISSIONS_TABLE + " WHERE account_id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, entity.getId());
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("permission_id");
                permissions.add(getById(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return permissions;
    }

    @Override
    public boolean deleteRelation(Account entity, Permission boundedEntity) throws DAOException {
        String query = "DELETE FROM " + ACCOUNT_PERMISSIONS_TABLE + " WHERE account_id = ? AND permission_id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, entity.getId());
            ps.setInt(2, boundedEntity.getId());
            return ps.executeUpdate() != 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
