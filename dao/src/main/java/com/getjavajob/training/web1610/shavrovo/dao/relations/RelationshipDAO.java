package com.getjavajob.training.web1610.shavrovo.dao.relations;

import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.util.Set;

/**
 * Created by saul on 20.04.2017.
 */
public interface RelationshipDAO<T> {
    void createRelation(T from, T to, RelationStatus status) throws DAOException;
    void deleteRelation(T from, T to, RelationStatus status) throws DAOException;
    Set<T> getAllRelatedFrom(T to, RelationStatus status) throws DAOException;
    Set<T> getAllRelatedTo(T to, RelationStatus status) throws DAOException;
    void createAllRelatedFrom(T from, RelationStatus status) throws DAOException;
    void createAllRelatedTo(T to, RelationStatus status) throws DAOException;

    enum RelationStatus {
        FRIEND, FRIEND_PENDING, SUBSCRIBER
    }
}
