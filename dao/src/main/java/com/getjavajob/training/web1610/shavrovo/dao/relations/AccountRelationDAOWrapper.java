package com.getjavajob.training.web1610.shavrovo.dao.relations;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO.RelationStatus.FRIEND_PENDING;
import static com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO.RelationStatus.SUBSCRIBER;

/**
 * Created by saul on 12.03.2017.
 */
public class AccountRelationDAOWrapper extends AbstractRelationDAO {

    public AccountRelationDAOWrapper() {
        super();
    }

    public Set<Account> getFriends(Account account) throws DAOException {
        return getAllRelatedTo(account, RelationStatus.FRIEND);
    }

    public void saveFriends(Account account) throws DAOException {
        createAllRelatedTo(account, RelationStatus.FRIEND);
    }

    public Set<Account> getMyFriendsPending(Account account) throws DAOException {
        return getAllRelatedFrom(account, FRIEND_PENDING);
    }

    public void saveMyFriendsPending(Account account) throws DAOException {
        createAllRelatedFrom(account, FRIEND_PENDING);
    }

    public Set<Account> getTheirFriendsPending(Account account) throws DAOException {
        return getAllRelatedTo(account, FRIEND_PENDING);
    }

    public void saveTheirFriendsPending(Account account) throws DAOException {
        createAllRelatedTo(account, FRIEND_PENDING);
    }

    public Set<Account> getSubscribers(Account account) throws DAOException {
        return getAllRelatedTo(account, SUBSCRIBER);
    }

    public void saveSubscribers(Account account) throws DAOException {
        createAllRelatedTo(account, SUBSCRIBER);
    }

    public Set<Account> getSubscriptions(Account account) throws DAOException {
        return getAllRelatedFrom(account, SUBSCRIBER);
    }

    public void saveSubscriptions(Account account) throws DAOException {
        createAllRelatedFrom(account, SUBSCRIBER);
    }
}
