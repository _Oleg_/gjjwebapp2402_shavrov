

CREATE TABLE IF NOT EXISTS Accounts (
  id INT AUTO_INCREMENT PRIMARY KEY,
  regTimestamp TIMESTAMP NOT NULL,
  email VARCHAR(30) UNIQUE NOT NULL,
  password VARCHAR(30) NOT NULL,
  firstName VARCHAR(20) NULL,
  lastName VARCHAR(20) NULL,
  middleName VARCHAR(20) NULL,
  birthDate DATE NULL,
  additionalInfo TEXT NULL,
  homeAddress VARCHAR(100) NULL,
  workAddress VARCHAR(100) NULL,
  avatarPath VARCHAR(100) NULL
);

CREATE TABLE IF NOT EXISTS Phones (
  id INT AUTO_INCREMENT PRIMARY KEY,
  login VARCHAR(50) NOT NULL,
  contact_type VARCHAR(50) NOT NULL,
  CONSTRAINT one_contact UNIQUE(id, login)
);

CREATE TABLE IF NOT EXISTS Messengers (
  id INT AUTO_INCREMENT PRIMARY KEY,
  login VARCHAR(50) NOT NULL,
  contact_type VARCHAR(50) NOT NULL,
  CONSTRAINT one_contact UNIQUE(id, login)
);

CREATE TABLE IF NOT EXISTS Account_phones (
  account_id INT NOT NULL,
  contact_id INT NOT NULL,
  FOREIGN KEY (account_id) REFERENCES Accounts(id) ON DELETE CASCADE,
  FOREIGN KEY (contact_id) REFERENCES Phones(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Account_messengers (
  account_id INT NOT NULL,
  contact_id INT NOT NULL,
  FOREIGN KEY (account_id) REFERENCES Accounts(id) ON DELETE CASCADE,
  FOREIGN KEY (contact_id) REFERENCES Messengers(id) ON DELETE CASCADE
);

-- Relationships tables --

CREATE TABLE IF NOT EXISTS Relationships_statuses (
  status VARCHAR(20) PRIMARY KEY
);

INSERT INTO Relationships_statuses (status)
VALUES ('SUBSCRIBER');

INSERT INTO Relationships_statuses (status)
VALUES ('FRIEND');

INSERT INTO Relationships_statuses (status)
VALUES ('FRIEND_PENDING');

CREATE TABLE IF NOT EXISTS Relationships (
  from_id int not null,
  to_id int not null,
  status VARCHAR(20) NOT NULL,
  one_relation int NULL,
  UNIQUE KEY one_relation (from_id, to_id, status),
  FOREIGN KEY (status) REFERENCES Relationships_statuses(status) ON DELETE CASCADE,
  FOREIGN KEY (from_id) REFERENCES Accounts(id) ON DELETE CASCADE,
  FOREIGN KEY (to_id) REFERENCES Accounts(id) ON DELETE CASCADE
);

-- end Relationships tables --

CREATE TABLE IF NOT EXISTS `Groups` (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Group_member_statuses (
  status VARCHAR(20) primary KEY
);

INSERT INTO Group_member_statuses (status) VALUE ('MEMBER');
INSERT INTO Group_member_statuses (status) VALUE ('ADMIN');
INSERT INTO Group_member_statuses (status) VALUE ('SUBSCRIBER');


CREATE TABLE IF NOT EXISTS Group_members (
  group_id int not null,
  account_id int not null,
  status VARCHAR(20) not null,
  FOREIGN KEY (status) REFERENCES Group_member_statuses(status) ON DELETE CASCADE,
  FOREIGN KEY (group_id) REFERENCES `Groups`(id) ON DELETE CASCADE,
  FOREIGN KEY (account_id) REFERENCES Accounts(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Permissions (
  id int primary key auto_increment,
  name varchar(30) not null,
  description varchar(140) not null
);

CREATE TABLE IF NOT EXISTS Account_permissions (
  account_id int not null,
  permission_id int not null,
  FOREIGN KEY (account_id) REFERENCES Accounts (id) ON DELETE CASCADE,
  FOREIGN KEY (permission_id) REFERENCES Permissions (id) ON DELETE CASCADE,
  CONSTRAINT unique_permission_to_account unique  (account_id, permission_id)
);

GRANT ALL PRIVILEGES ON webapp.* TO 'jdbc_user'@'%' identified by 'jdbc_password123';

