use webapp;

drop table Account_phones;
drop table Account_messengers;
drop table Phones;
drop table Messengers;
drop table relationships;
drop table relationships_statuses;
drop table group_members;
drop table group_member_statuses;
drop table groups;
drop table accounts;

show tables;