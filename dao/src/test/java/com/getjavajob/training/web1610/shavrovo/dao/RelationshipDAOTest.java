package com.getjavajob.training.web1610.shavrovo.dao;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import com.getjavajob.training.web1610.shavrovo.dao.relations.AccountRelationDAOWrapper;
import com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by saul on 5/4/2017.
 */
class RelationshipDAOTest {
    private static EntityDAO<Account> accountDAO;
    private static RelationshipDAO<Account> relationshipDAO;
    private static int testCount;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;
    private Account account5;

    @BeforeAll
    static void initializeConnection() {
        TestDBHelper.initializeTables();
        accountDAO = new AccountDAO();
        relationshipDAO = new AccountRelationDAOWrapper();
    }

    @BeforeEach
    void setIds() throws Exception {
        AccountBuilder builder = new AccountBuilder();
        String email1 = "email" + testCount++ + "@mail.com";
        String password1 = "password1";
        account1 = accountDAO.create(builder.email(email1).password(password1).createAccount());
        String email2 = "email" + testCount++ + "@mail.com";
        String password2 = "password2";
        account2 = accountDAO.create(builder.email(email2).password(password2).createAccount());
        String email3 = "email" + testCount++ + "@mail.com";
        String password3 = "password3";
        account3 = accountDAO.create(builder.email(email3).password(password3).createAccount());
        String email4 = "email" + testCount++ + "@mail.com";
        String password4 = "password4";
        account4 = accountDAO.create(builder.email(email4).password(password4).createAccount());
        String email5 = "email" + testCount++ + "@mail.com";
        String password5 = "password5";
        account5 = accountDAO.create(builder.email(email5).password(password5).createAccount());
    }

    @AfterAll
    static void deleteTables() {
        TestDBHelper.dropTables();
    }


    @Test
    void createRelation() throws DAOException {
        relationshipDAO.createRelation(account1, account2, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        relationshipDAO.createRelation(account1, account3, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        relationshipDAO.createRelation(account1, account4, RelationshipDAO.RelationStatus.FRIEND_PENDING);

        Set<Account> expected = new HashSet<>(Arrays.asList(account2, account3, account4));
        Set<Account> actual = relationshipDAO.getAllRelatedFrom(account1, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        assertEquals(expected, actual);
    }

    @Test
    void deleteRelation() throws DAOException {
        relationshipDAO.createRelation(account1, account2, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        relationshipDAO.createRelation(account1, account3, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        relationshipDAO.createRelation(account1, account4, RelationshipDAO.RelationStatus.FRIEND_PENDING);

        relationshipDAO.deleteRelation(account1, account4, RelationshipDAO.RelationStatus.FRIEND_PENDING);

        Set<Account> expected = new HashSet<>(Arrays.asList(account2, account3));
        Set<Account> actual = relationshipDAO.getAllRelatedFrom(account1, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        assertEquals(expected, actual);
    }

    @Test
    void getAllRelatedFrom() throws DAOException {
        relationshipDAO.createRelation(account1, account3, RelationshipDAO.RelationStatus.FRIEND);
        relationshipDAO.createRelation(account1, account4, RelationshipDAO.RelationStatus.FRIEND);
        relationshipDAO.createRelation(account1, account5, RelationshipDAO.RelationStatus.FRIEND);

        Set<Account> expected = new HashSet<>(Arrays.asList(account3, account4, account5));
        Set<Account> actual = relationshipDAO.getAllRelatedFrom(account1, RelationshipDAO.RelationStatus.FRIEND);
        assertEquals(expected, actual);
    }

    @Test
    void createAllRelatedFrom() throws DAOException {
        account1.subscribe(account2);
        account1.subscribe(account3);
        relationshipDAO.createRelation(account4, account1, RelationshipDAO.RelationStatus.FRIEND);

        // Сохранить всех, на кого подписался account1
        relationshipDAO.createAllRelatedFrom(account1, RelationshipDAO.RelationStatus.SUBSCRIBER);

        Set<Account> expected = new HashSet<>(Arrays.asList(account2, account3));
        Set<Account> actual = relationshipDAO.getAllRelatedFrom(account1, RelationshipDAO.RelationStatus.SUBSCRIBER);
        assertEquals(expected, actual);
    }

    @Test
    void createAllRelatedTo() throws DAOException {
        account2.sendFriendshipRequest(account1);
        account3.sendFriendshipRequest(account1);
        account4.sendFriendshipRequest(account1);

        // Сохранить всех, на кто отправил запрос в друзья к account1
        relationshipDAO.createAllRelatedTo(account1, RelationshipDAO.RelationStatus.FRIEND_PENDING);

        Set<Account> expected = new HashSet<>(Arrays.asList(account2, account3, account4));
        Set<Account> actual = relationshipDAO.getAllRelatedTo(account1, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        assertEquals(expected, actual);
    }

}