package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.Permission;
import com.getjavajob.training.web1610.shavrovo.contacts.*;
import com.getjavajob.training.web1610.shavrovo.dao.TestDBHelper;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import org.junit.jupiter.api.*;

import java.sql.*;
import java.util.*;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.PERMISSIONS_TABLE;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by saul on 5/9/2017.
 */
class PermissionsDAOTest {
    Account account;
    AccountDAO accountDAO;
    PermissionDAO permissionDAO;
    Permission permission;
    static int testCount;

    private Permission createPermission(String name, String description) {
        Permission permission = null;
        String insertQuery = "INSERT INTO " + PERMISSIONS_TABLE + " (name, description) VALUES (?, ?)";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, name);
            ps.setString(2, description);
            ps.executeUpdate();
            ResultSet resultSet = ps.getGeneratedKeys();
            if (resultSet.next()) {
                int id = resultSet.getInt(1);
                permission = new Permission(id, name, description);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return permission;
    }

    @BeforeAll
    static void createTables() {
        TestDBHelper.initializeTables();
    }

    @AfterAll
    static void dropTables() {
        TestDBHelper.dropTables();
    }

    @BeforeEach
    void setUp() {
        account = new AccountBuilder().email("account" + ++testCount + "@mail.ru").password("password").createAccount();
        permission = createPermission("name" + testCount, "descr" + testCount);

        accountDAO = new AccountDAO();
        permissionDAO = new PermissionDAO();
    }

    @AfterEach
    void tearDown() {
        account = null;
//        permission = null;
    }


    @Test
    void getAllBounded() throws Exception {
        Account createdAccount = accountDAO.create(account);
        createdAccount.addPermission(permission);
        permissionDAO.createRelation(createdAccount, permission);
        List<Permission> expected = Arrays.asList(permission);
        List<Permission> actual = new ArrayList<>(permissionDAO.getAllBounded(createdAccount));

        assertEquals(expected.get(0).getId(), actual.get(0).getId());
        assertEquals(expected.get(0).getName(), actual.get(0).getName());
        assertEquals(expected.get(0).getDescription(), actual.get(0).getDescription());
    }

    @Test
    void deleteBounded() throws Exception {
        Account createdAccount = accountDAO.create(account);
        createdAccount.addPermission(permission);
        permissionDAO.createRelation(createdAccount, permission);

        assertTrue(permissionDAO.deleteRelation(createdAccount, permission));
        assertEquals(new HashSet<>(), permissionDAO.getAllBounded(createdAccount));
    }

}
