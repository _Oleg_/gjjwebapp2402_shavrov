package com.getjavajob.training.web1610.shavrovo.dao;

import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.*;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.ACCOUNTS_TABLE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Created by saul on 27.02.2017.
 */
class DBInitializerTest {

    @BeforeAll
    static void initializeConnection() {
        TestDBHelper.initializeTables();
    }

    @AfterAll
    static void dropTables() {
        TestDBHelper.dropTables();
    }

    @Test
    void getConnection() {
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT 2 + 2;");
            int result = 0;
            while (resultSet.next()) {
                result = resultSet.getInt(1);
            }
            ConnectionPool.getInstance().commit();
            assertEquals(result, 4);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
    }

    @Test
    void executeQueryToH2() {
        String email = "email@mail.com";
        String password = "password";

        String query = "INSERT INTO " + ACCOUNTS_TABLE + " (regTimestamp, email, password) VALUES (?, ?, ?)";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (
                Statement statement = connection.createStatement();
                PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)
        ){

            ps.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
            ps.setString(2, email);
            ps.setString(3, password);
            ps.executeUpdate();

            ResultSet generatedKeys = ps.getGeneratedKeys();
            int id = 0;
            if (generatedKeys.next()) {
                id = generatedKeys.getInt(1);
            }
            ResultSet resultSet = statement.executeQuery("Select * from " + ACCOUNTS_TABLE + " where id = " + id);
            int result = 0;
            String emailFromDB = null;
            while (resultSet.next()) {
                result = resultSet.getInt("id");
                emailFromDB = resultSet.getString("email");
            }
            ConnectionPool.getInstance().commit();
            assertEquals(id, result);
            assertEquals(email, emailFromDB);
        } catch(SQLException e){
            e.printStackTrace();
            assertFalse(true);
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
    }
}