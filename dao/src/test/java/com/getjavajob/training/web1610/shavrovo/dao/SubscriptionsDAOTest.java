package com.getjavajob.training.web1610.shavrovo.dao;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.DBInitializer;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import com.getjavajob.training.web1610.shavrovo.dao.relations.AccountRelationDAOWrapper;
import org.junit.jupiter.api.*;

import java.sql.*;
import java.util.*;

import static com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO.RelationStatus.FRIEND;
import static com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO.RelationStatus.SUBSCRIBER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by saul on 13.03.2017.
 */
class SubscriptionsDAOTest {
    private static EntityDAO<Account> accountDAO;
    private static AccountRelationDAOWrapper subscriptionsDAO;
    private static int testCount;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;
    private Account account5;

    @BeforeAll
    static void initializeConnection() {
        accountDAO = new AccountDAO();
        subscriptionsDAO = new AccountRelationDAOWrapper();
    }

    @BeforeEach
    void setIds() throws Exception {
        TestDBHelper.initializeTables();

        List<Account> accountList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AccountBuilder builder = new AccountBuilder();
            String email1 = "email" + ++testCount + "@mail.com";
            String password1 = "password";
            accountList.add(accountDAO.create(builder.email(email1).password(password1).createAccount()));
        }
        account1 = accountList.get(0);
        account2 = accountList.get(1);
        account3 = accountList.get(2);
        account4 = accountList.get(3);
        account5 = accountList.get(4);
    }

    @AfterEach
    void afterEach() {
        TestDBHelper.dropTables();
    }

    @Test
    void getFriends() throws DAOException {
        account2.sendFriendshipRequest(account1);
        account1.acceptFriendRequest(account2);

        account3.sendFriendshipRequest(account1);
        account1.acceptFriendRequest(account3);

        account4.sendFriendshipRequest(account1);
        account1.acceptFriendRequest(account4);

        subscriptionsDAO.saveFriends(account1);
        ConnectionPool.getInstance().commit();
        assertEquals(new HashSet<>(Arrays.asList(account2, account3, account4)), account1.getFriends());
        assertEquals(new HashSet<>(Arrays.asList(account2, account3, account4)), subscriptionsDAO.getFriends(account1));

        subscriptionsDAO.saveFriends(account2);
        ConnectionPool.getInstance().commit();
        assertEquals(new HashSet<>(Arrays.asList(account1)), subscriptionsDAO.getFriends(account2));
    }

    @Test
    void saveFriends() throws DAOException {
        account1.sendFriendshipRequest(account2);
        account2.acceptFriendRequest(account1);
        account1.sendFriendshipRequest(account3);
        account3.acceptFriendRequest(account1);
        account1.sendFriendshipRequest(account4);
        account4.acceptFriendRequest(account1);

        subscriptionsDAO.saveFriends(account1);
        ConnectionPool.getInstance().commit();
        Set<Account> friendsOfAccount1 = subscriptionsDAO.getFriends(account1);
        assertEquals(new HashSet<>(Arrays.asList(account2, account3, account4)), friendsOfAccount1);
    }

    @Test
    void saveAndGetMyFriendsPending() throws DAOException {
        account1.sendFriendshipRequest(account2);
        subscriptionsDAO.saveMyFriendsPending(account1);
        ConnectionPool.getInstance().commit();
        assertThat(subscriptionsDAO.getMyFriendsPending(account1), equalTo(account1.getMyFriendsPending()));
    }

    @Test
    void saveAndGetTheirFriendsPending() throws DAOException {
        account1.sendFriendshipRequest(account2);
        subscriptionsDAO.saveTheirFriendsPending(account2);
        ConnectionPool.getInstance().commit();
        assertThat(subscriptionsDAO.getMyFriendsPending(account1), equalTo(account1.getMyFriendsPending()));
        assertThat(subscriptionsDAO.getTheirFriendsPending(account2), equalTo(account2.getTheirFriendsPending()));
    }

    @Test
    void getSubscribers() throws DAOException {
        account1.subscribe(account5);
        account2.subscribe(account5);
        account3.subscribe(account5);
        account4.subscribe(account5);

        String subscribersInsertionQuery = "INSERT INTO " + DBInitializer.RELATIONSHIPS_TABLE + " (from_id, to_id, status) " +
                "VALUES " +
                "(?, ?, ?);";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(subscribersInsertionQuery)) {
            for (Account account : new Account[]{account1, account2, account3, account4}) {
                ps.setInt(1, account.getId());
                ps.setInt(2, account5.getId());
                ps.setString(3, SUBSCRIBER.name());
                ps.executeUpdate();
                ConnectionPool.getInstance().commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
        Set<Account> subscribers = subscriptionsDAO.getSubscribers(account5);
        ConnectionPool.getInstance().commit();
        assertEquals(account5.getSubscribers(), subscribers);
    }

    @Test
    void saveSubscribers() throws Exception {
        account1.subscribe(account5);
        account2.subscribe(account5);
        account3.subscribe(account5);
        account4.subscribe(account5);
        subscriptionsDAO.saveSubscribers(account5);

        String query = "SELECT DISTINCT from_id FROM " + DBInitializer.RELATIONSHIPS_TABLE + " WHERE to_id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, account5.getId());
            ResultSet resultSet = ps.executeQuery();
            Set<Account> subscribers = new HashSet<>();
            while (resultSet.next()) {
                Account subscriber = accountDAO.getById(resultSet.getInt("from_id"));
                subscribers.add(subscriber);
            }
            ConnectionPool.getInstance().commit();
            assertEquals(account5.getSubscribers(), subscribers);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
    }

    @Test
    void getSubscriptions() throws Exception {
        account1.subscribe(account2);
        account1.subscribe(account3);
        String query = "SELECT to_id FROM " + DBInitializer.RELATIONSHIPS_TABLE + " WHERE from_id = ?";
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, account1.getId());
            ResultSet resultSet = ps.executeQuery();
            Set<Account> subscriptions = new HashSet<>();
            while (resultSet.next()) {
                Account subscription = accountDAO.getById(resultSet.getInt("to_id"));
                subscriptions.add(subscription);
            }
            ConnectionPool.getInstance().commit();
            assertThat(subscriptions, equalTo(subscriptionsDAO.getSubscriptions(account1)));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
    }

    @Test
    void saveSubscriptions() throws Exception {
        account1.subscribe(account2);
        account1.subscribe(account3);
        subscriptionsDAO.saveSubscriptions(account1);
        String query = "SELECT to_id FROM " + DBInitializer.RELATIONSHIPS_TABLE + " WHERE from_id = ?";
        Set<Account> subscriptions = new HashSet<>();
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, account1.getId());
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Account subscription = accountDAO.getById(resultSet.getInt("to_id"));
                subscriptions.add(subscription);
            }
            ConnectionPool.getInstance().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
        assertThat(account1.getSubscriptions(), equalTo(subscriptions));
    }
}