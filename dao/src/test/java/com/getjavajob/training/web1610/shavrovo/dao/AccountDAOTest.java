package com.getjavajob.training.web1610.shavrovo.dao;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import org.junit.jupiter.api.*;

import java.sql.*;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIn.in;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by saul on 28.02.2017.
 */
class AccountDAOTest {
    private static int testCount;
    private static EntityDAO<Account> accountDAO;
    private Account rawAccount;
    private String email;
    private String password;

    @BeforeAll
    static void setConnection() {
        TestDBHelper.initializeTables();
        accountDAO = new AccountDAO();
    }

    @BeforeEach
    void createAccount() {
        email = "afj22332" + ++testCount + "@mail.com";
        password = "jf32049";
        AccountBuilder builder = new AccountBuilder();
        rawAccount = builder
                .email(email)
                .password(password)
                .createAccount();
    }

    @AfterEach
    void resetRawAccount() {
        rawAccount = null;
    }

    @AfterAll
    static void deleteTables() {
        TestDBHelper.dropTables();
    }

    @Test
    void getAll() throws Exception {
        AccountBuilder builder = new AccountBuilder();
        Account account1 = accountDAO.create(builder.email("email@mail.com").password("password").createAccount());
        Account account2 = accountDAO.create(builder.email("email2@gmail.com").password("password").createAccount());
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        Set<Account> accounts = accountDAO.getAll();
        assertThat(account1, in(accounts));
        assertThat(account2, in(accounts));
    }

    @Test
    void getById() throws Exception {
        Account account = accountDAO.create(rawAccount);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        int id = account.getId();

        Account fromDB = accountDAO.getById(id);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        assertThat(id, equalTo(fromDB.getId()));
        assertThat(email, equalTo(fromDB.getEmail()));
        assertThat(password, equalTo(fromDB.getPassword()));
    }

    @Test
    void create() throws Exception {
        Account createdAccount = accountDAO.create(rawAccount);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        assertNotNull(createdAccount);
        String query = String.format("SELECT * FROM %s WHERE id = ?", ACCOUNTS_TABLE);
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, createdAccount.getId());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                assertThat(createdAccount.getId(), equalTo(rs.getInt("id")));
                assertThat(createdAccount.getEmail(), equalTo(rs.getString("email")));
                assertThat(createdAccount.getPassword(), equalTo(rs.getString("password")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
    }

    @Test
    void update() throws Exception {
        Account account = accountDAO.create(rawAccount);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();

        account.setEmail("fwjoefwoiej@mail.ru");
        Account updatedAccount = accountDAO.update(account);
        Account fromDB = accountDAO.getById(account.getId());
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();

        assertThat(fromDB.getId(), equalTo(updatedAccount.getId()));
        assertThat(fromDB.getEmail(), equalTo(updatedAccount.getEmail()));
        assertThat(fromDB.getPassword(), equalTo(updatedAccount.getPassword()));
    }

    @Test
    void delete() throws Exception {
        Account account = accountDAO.create(rawAccount);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        int id = account.getId();
        accountDAO.delete(account);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format("SELECT * FROM %s WHERE id = %d", ACCOUNTS_TABLE, id));
            assertFalse(resultSet.next());
            if (resultSet.next()) {
                System.out.println(resultSet.getInt("id"));
                System.out.println(resultSet.getString("email"));
                System.out.println(resultSet.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
    }

    @Test
    void getAccountsByFirstName() throws DAOException {
        String firstName = "Barry";
        rawAccount.setFirstName(firstName);
        Account account = accountDAO.create(rawAccount);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        Set<Account> accounts = accountDAO.getAllByKeyValuePair("firstName", firstName);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();

        assertTrue(accounts.contains(account));
    }
}