package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.contacts.*;
import com.getjavajob.training.web1610.shavrovo.dao.TestDBHelper;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by saul on 5/9/2017.
 */
class MessengerDAOTest {
    Account account;
    Messenger telegramContact;
    MessengerDAO messengerDAO;
    EntityDAO<Account> accountDAO;

    @BeforeAll
    static void createTables() {
        TestDBHelper.initializeTables();
    }

    @AfterAll
    static void dropTables() {
        TestDBHelper.dropTables();
    }

    @BeforeEach
    void setUp() {
        account = new AccountBuilder().email("account@mail.ru").password("password").createAccount();
        telegramContact = new Messenger("89244547515", MessengerType.TELEGRAM);
        account.setMessengers(new HashSet<>(Collections.singleton(telegramContact)));
        messengerDAO = new MessengerDAO();
        accountDAO = new AccountDAO();
    }

    @AfterEach
    void tearDown() {
        telegramContact = null;
    }

    @Test
    void create() throws Exception {
        Messenger messenger = messengerDAO.create(telegramContact);
        ConnectionPool.getInstance().commit();
        assertNotNull(messenger.getId());
        assertNotEquals(0, messenger.getId());
    }

    @Test
    void getById() throws Exception {
        Messenger messenger = messengerDAO.create(telegramContact);
        ConnectionPool.getInstance().commit();
        Messenger fromDB = messengerDAO.getById(messenger.getId());
        assertEquals(messenger.getId(), fromDB.getId());
        assertEquals(messenger.getContactType(), fromDB.getContactType());
        assertEquals(messenger.getLogin(), fromDB.getLogin());
    }

    @Test
    void update() throws Exception {
        Messenger messenger = messengerDAO.create(telegramContact);
        ConnectionPool.getInstance().commit();
        Messenger fromDB = messengerDAO.getById(messenger.getId());
        fromDB.setContactType(MessengerType.TELEGRAM);
        fromDB.setLogin("00000000000");
        Messenger updated = messengerDAO.update(fromDB);
        Messenger updatedFromDB = messengerDAO.getById(updated.getId());

        assertEquals(updated.getId(), updatedFromDB.getId());
        assertEquals(updated.getLogin(), updatedFromDB.getLogin());
        assertEquals(updated.getContactType(), updatedFromDB.getContactType());
    }

    @Test
    void delete() throws Exception {
        Messenger messenger = messengerDAO.create(telegramContact);
        ConnectionPool.getInstance().commit();
        assertTrue(messengerDAO.delete(messenger));
        assertNull(messengerDAO.getById(messenger.getId()));
    }

    @Test
    void createRelation() throws Exception {
        Account createdAccount = accountDAO.create(account);
        Messenger createdMessenger = messengerDAO.create(telegramContact);
        createdAccount.addMessenger(createdMessenger);
        messengerDAO.createRelation(createdAccount, createdMessenger);
        List<Contact> expected = Arrays.asList(createdMessenger);
        List<Contact> actual = new ArrayList<>(messengerDAO.getAllBounded(createdAccount));
        assertEquals(expected.get(0).getId(), actual.get(0).getId());
        assertEquals(expected.get(0).getLogin(), actual.get(0).getLogin());
        assertEquals(expected.get(0).getContactType(), actual.get(0).getContactType());
    }

}
