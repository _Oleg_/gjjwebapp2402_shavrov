package com.getjavajob.training.web1610.shavrovo.dao;

import com.getjavajob.training.web1610.shavrovo.*;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import org.junit.jupiter.api.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIn.in;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Created by saul on 15.03.2017.
 */
class GroupDAOTest {
    private static GroupDAO groupDAO;
    private static EntityDAO<Account> accountDAO;

    private static int testCount;
    private String groupName;
    private Group group = null;
    private Account member1;
    private Account member2;
    private Account member3;
    private Account admin;

    @BeforeAll
    static void setConnection() {
        TestDBHelper.initializeTables();
        groupDAO = new GroupDAO();
        accountDAO = new AccountDAO();
    }

    @BeforeEach
    void prepareGroup() throws DAOException {
        groupName = "Awesome group";
        Group rawGroup = new GroupBuilder(groupName).createGroup();

        AccountBuilder builder = new AccountBuilder();
        member1 = accountDAO.create(builder.email("email" + testCount++ + "@site.com").password("password").createAccount());
        member2 = accountDAO.create(builder.email("email" + testCount++ + "@site.com").password("password").createAccount());
        member3 = accountDAO.create(builder.email("email" + testCount++ + "@site.com").password("password").createAccount());
        admin = accountDAO.create(builder.email("email" + testCount++ + "@site.com").password("password").createAccount());

        rawGroup.addMember(admin);
        rawGroup.assignAdmin(admin);
        rawGroup.addMember(member1);
        rawGroup.addMember(member2);
        rawGroup.addMember(member3);

        group = groupDAO.create(rawGroup);
    }

    @AfterEach
    void tearDownGroup() {
        groupName = null;
        group = null;
        member1 = null;
        member2 = null;
        member3 = null;
        admin = null;
    }

    @AfterAll
    static void deleteTables() {
        TestDBHelper.dropTables();
    }

    @Test
    void create() {
        Group recreatedGroup = groupDAO.create(group);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        Group fromDB = groupDAO.getById(recreatedGroup.getId());
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        assertThat(admin, in(fromDB.getAdmins()));
        assertThat(admin, in(fromDB.getMembers()));
    }

    @Test
    void getById() {
        Group groupFromDB = groupDAO.getById(group.getId());
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        assertEquals(group.getId(), groupFromDB.getId());
        assertEquals(group.getName(), groupFromDB.getName());
        assertEquals(group.getAdmins(), groupFromDB.getAdmins());
        assertEquals(group.getMembers(), groupFromDB.getMembers());
    }

    @Test
    void update() {
        group.assignAdmin(member1);
        group.setName("New awesome name");
        groupDAO.update(group);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();

        Group groupFromDB = groupDAO.getById(group.getId());
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();

        assertEquals(group.getId(), groupFromDB.getId());
        assertEquals(group.getName(), groupFromDB.getName());
        assertEquals(group.getAdmins(), groupFromDB.getAdmins());
        assertEquals(group.getMembers(), groupFromDB.getMembers());
    }

    @Test
    void delete() {
        GroupDAO groupDAO = new GroupDAO();
        groupDAO.create(group);
        groupDAO.delete(group);
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();

        Group groupFromDB = groupDAO.getById(group.getId());
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        assertNull(groupFromDB);
    }

}