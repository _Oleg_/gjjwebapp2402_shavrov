package com.getjavajob.training.web1610.shavrovo.dao;

import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.DBInitializer;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.*;
import static com.getjavajob.training.web1610.shavrovo.DBInitializer.GROUPS_TABLE;
import static com.getjavajob.training.web1610.shavrovo.DBInitializer.GROUP_MEMBERS_TABLE;

/**
 * Created by saul on 03.03.2017.
 */
public class TestDBHelper {
    private static final String TEST_TABLES_INIT_SCRIPT = "test.sql";

    public static void initializeTables() {
        DBInitializer.createTables(TEST_TABLES_INIT_SCRIPT);
    }

    public static void dropTables() {
        Connection connection = ConnectionPool.getInstance().acquireConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(String.format("drop table %s;", ACCOUNTS_TABLE));
            statement.executeUpdate(String.format("drop table %s;", RELATIONSHIPS_TABLE));
            statement.executeUpdate(String.format("drop table %s;", PHONES_TABLE));
            statement.executeUpdate(String.format("drop table %s;", MESSENGERS_TABLE));
            statement.executeUpdate(String.format("drop table %s;", ACCOUNT_PHONES_TABLE));
            statement.executeUpdate(String.format("drop table %s;", ACCOUNT_MESSENGERS_TABLE));
            statement.executeUpdate(String.format("drop table %s;", GROUPS_TABLE));
            statement.executeUpdate(String.format("drop table %s;", GROUP_MEMBERS_TABLE));
            statement.executeUpdate(String.format("drop table %s;", PERMISSIONS_TABLE));
            statement.executeUpdate(String.format("drop table %s;", ACCOUNT_PERMISSIONS_TABLE));
            ConnectionPool.getInstance().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().releaseConnection();
        }
    }
}
