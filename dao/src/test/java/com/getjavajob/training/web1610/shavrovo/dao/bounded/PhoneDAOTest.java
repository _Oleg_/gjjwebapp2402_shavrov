package com.getjavajob.training.web1610.shavrovo.dao.bounded;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.ConnectionPool;
import com.getjavajob.training.web1610.shavrovo.contacts.*;
import com.getjavajob.training.web1610.shavrovo.dao.TestDBHelper;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by saul on 5/9/2017.
 */
class PhoneDAOTest {
    Account account;
    Phone personalPhoneContact;
    Messenger telegramContact;
    PhoneDAO phonesDAO;
    EntityDAO<Account> accountDAO;
    static int testCount;

    @BeforeAll
    static void createTables() {
        TestDBHelper.initializeTables();
    }

    @AfterAll
    static void dropTables() {
        TestDBHelper.dropTables();
    }

    @BeforeEach
    void setUp() {
        account = new AccountBuilder().email("account" + ++testCount + "@mail.ru").password("password").createAccount();
        telegramContact = new Messenger("89244547515", MessengerType.TELEGRAM);
        personalPhoneContact = new Phone("89999999999", PhoneType.PERSONAL);
        account.setPhones(new HashSet<>(Collections.singleton(personalPhoneContact)));
        account.setMessengers(new HashSet<>(Collections.singleton(telegramContact)));
        phonesDAO = new PhoneDAO();
        accountDAO = new AccountDAO();
    }

    @AfterEach
    void tearDown() {
        telegramContact = null;
    }

    @Test
    void create() throws Exception {
        Phone phone = phonesDAO.create(personalPhoneContact);
        ConnectionPool.getInstance().commit();
        assertNotNull(phone.getId());
        assertNotEquals(0, phone.getId());
    }

    @Test
    void getById() throws Exception {
        Phone phone = phonesDAO.create(personalPhoneContact);
        ConnectionPool.getInstance().commit();
        Phone fromDB = phonesDAO.getById(phone.getId());
        assertEquals(phone.getId(), fromDB.getId());
        assertEquals(phone.getContactType(), fromDB.getContactType());
        assertEquals(phone.getLogin(), fromDB.getLogin());
    }

    @Test
    void update() throws Exception {
        Phone phone = phonesDAO.create(personalPhoneContact);
        ConnectionPool.getInstance().commit();
        Phone fromDB = phonesDAO.getById(phone.getId());
        fromDB.setContactType(PhoneType.ADDITIONAL);
        fromDB.setLogin("00000000000");
        Phone updated = phonesDAO.update(fromDB);
        Phone updatedFromDB = phonesDAO.getById(updated.getId());

        assertEquals(updated.getId(), updatedFromDB.getId());
        assertEquals(updated.getLogin(), updatedFromDB.getLogin());
        assertEquals(updated.getContactType(), updatedFromDB.getContactType());
    }

    @Test
    void delete() throws Exception {
        Phone phone = phonesDAO.create(personalPhoneContact);
        ConnectionPool.getInstance().commit();
        assertTrue(phonesDAO.delete(phone));
        assertNull(phonesDAO.getById(phone.getId()));
    }

    @Test
    void getAllBounded() throws Exception {
        Account createdAccount = accountDAO.create(account);
        Phone createdContact = phonesDAO.create(personalPhoneContact);
        createdAccount.addPhone(createdContact);
        phonesDAO.createRelation(createdAccount, createdContact);
        List<Phone> expected = Arrays.asList(createdContact);
        List<Phone> actual = new ArrayList<>(phonesDAO.getAllBounded(createdAccount));

        assertEquals(expected.get(0).getId(), actual.get(0).getId());
        assertEquals(expected.get(0).getLogin(), actual.get(0).getLogin());
        assertEquals(expected.get(0).getContactType(), actual.get(0).getContactType());
    }

    @Test
    void deleteBounded() throws Exception {
        Account createdAccount = accountDAO.create(account);
        Phone createdContact = phonesDAO.create(personalPhoneContact);
        createdAccount.addPhone(createdContact);
        phonesDAO.createRelation(createdAccount, createdContact);
        List<Contact> expected = Arrays.asList(createdContact);
        List<Contact> actual = new ArrayList<>(phonesDAO.getAllBounded(createdAccount));

        assertEquals(expected.get(0).getId(), actual.get(0).getId());
        assertEquals(expected.get(0).getLogin(), actual.get(0).getLogin());
        assertEquals(expected.get(0).getContactType(), actual.get(0).getContactType());

        assertTrue(phonesDAO.deleteRelation(createdAccount, createdContact));
        assertEquals(new HashSet<>(), phonesDAO.getAllBounded(createdAccount));
    }
}
