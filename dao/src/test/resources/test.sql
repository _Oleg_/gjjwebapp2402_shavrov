
CREATE TABLE IF NOT EXISTS Accounts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    regTimestamp TIMESTAMP NOT NULL,
    email VARCHAR(30) UNIQUE NOT NULL,
    password VARCHAR(30) NOT NULL,
    firstName VARCHAR(20) NULL,
    lastName VARCHAR(20) NULL,
    middleName VARCHAR(20) NULL,
    birthDate DATE NULL,
    additionalInfo TEXT NULL,
    homeAddress VARCHAR(100) NULL,
    workAddress VARCHAR(100) NULL,
    avatarPath VARCHAR(100) NULL
);

CREATE TABLE IF NOT EXISTS Phones (
    id INT AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(50) NOT NULL,
    contact_type VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Messengers (
    id INT AUTO_INCREMENT PRIMARY KEY,
    login VARCHAR(50) NOT NULL,
    contact_type VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Account_phones (
    account_id INT NOT NULL,
    contact_id INT NOT NULL,
    FOREIGN KEY (account_id) REFERENCES Accounts(id) ON DELETE CASCADE,
    FOREIGN KEY (contact_id) REFERENCES Phones(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Account_messengers (
    account_id INT NOT NULL,
    contact_id INT NOT NULL,
    FOREIGN KEY (account_id) REFERENCES Accounts(id) ON DELETE CASCADE,
    FOREIGN KEY (contact_id) REFERENCES Messengers(id) ON DELETE CASCADE
);

-- Relationships tables --

CREATE DOMAIN IF NOT EXISTS Relationships_statuses AS VARCHAR(20)
    CHECK (VALUE IN ('SUBSCRIBER', 'FRIEND', 'FRIEND_PENDING'));

CREATE TABLE IF NOT EXISTS Relationships (
    from_id int not null,
    to_id int not null,
    status Relationships_statuses NOT NULL,
    FOREIGN KEY (from_id) REFERENCES Accounts(id) ON DELETE CASCADE,
    FOREIGN KEY (to_id) REFERENCES Accounts(id) ON DELETE CASCADE
);

-- end Relationships tables --

CREATE TABLE IF NOT EXISTS `Groups` (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL
);

CREATE DOMAIN IF NOT EXISTS Group_member_statuses AS VARCHAR(20)
    DEFAULT 'MEMBER'
    CHECK (VALUE IN ('ADMIN', 'MEMBER', 'SUBSCRIBER'));

CREATE TABLE IF NOT EXISTS Group_members (
    group_id int not null,
    account_id int not null,
    status Group_member_statuses not null,
    FOREIGN KEY (group_id) REFERENCES `Groups`(id) ON DELETE CASCADE,
    FOREIGN KEY (account_id) REFERENCES Accounts(id) ON DELETE CASCADE,
);

CREATE TABLE IF NOT EXISTS Permissions (
    id int primary key auto_increment,
    name varchar(30) not null,
    description varchar(140) not null
);

CREATE TABLE IF NOT EXISTS Account_permissions (
    account_id int not null,
    permission_id int not null,
    FOREIGN KEY (account_id) REFERENCES Accounts (id) ON DELETE CASCADE,
    FOREIGN KEY (permission_id) REFERENCES Permissions (id) ON DELETE CASCADE,
    CONSTRAINT unique_permission_to_account unique  (account_id, permission_id)
);

