package com.getjavajob.training.web1610.shavrovo.servlets.auth;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToAccountPage;
import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToMainPage;

/**
 * Created by saul on 4/24/2017.
 */
public class LoginServlet extends HttpServlet {
    public static final String EMAIL_TO_HASHES_MAP_NAME = "emailHashes";
    private static String userIDCookieName;
    private AccountService accountService;

    @Override
    public void init() throws ServletException {
        super.init();
        userIDCookieName = getServletContext().getInitParameter("userIDCookieName");
        accountService = new AccountService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(true);
        Map<String, String> loginFormParams = getLoginFormParams(req);

        String email = loginFormParams.get("signInEmail");
        String password = loginFormParams.get("signInPassword");
        boolean rememberMe = loginFormParams.get("signInRememberMe") != null;

        Account account = accountService.findAccount(email, password);
        if (account != null) {
            System.out.println("You're logged in as " + account);
            session.setAttribute("account", account);
            if (rememberMe) {
                addLoginCookie(req, resp, email);
            } else {
                removeAnyUserCookies(req, resp);
            }
            // TODO: 5/3/2017 Redirect to previous page
            redirectToAccountPage(req, resp);
        } else {
            // TODO: 5/1/2017 Выдавать сообщение пользователю о неправильном логине или пароле
            // TODO: 5/1/2017 Redirect to login page with error message
            System.out.println("from wrong login params of doPost() " + getClass().getName());
            req.getSession().setAttribute("wrongInput", loginFormParams);
            redirectToMainPage(req, resp);
        }
    }

    private Map<String, String> getLoginFormParams(HttpServletRequest req) {
        Map<String, String> loginFormParams = new HashMap<>();
        loginFormParams.put("signInEmail", req.getParameter("signInEmail"));
        loginFormParams.put("signInPassword", req.getParameter("signInPassword"));
        loginFormParams.put("signInRememberMe", req.getParameter("signInRememberMe"));
        return loginFormParams;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (loginByCookie(req)) {
            resp.sendRedirect("/WEB-INF/jsp/account_info.jsp");
        } else {
            redirectToMainPage(req, resp);
        }
    }

    @SuppressWarnings("unchecked")
    private void addLoginCookie(HttpServletRequest req, HttpServletResponse resp, String email) {
        String secret = accountService.getSecret();
        String emailHash = String.valueOf((email + secret).hashCode());
        ServletContext servletContext = req.getServletContext();
        Map<String, String> emailHashesForCookie = (Map<String, String>) servletContext.getAttribute(EMAIL_TO_HASHES_MAP_NAME);
        if (emailHashesForCookie == null) {
            emailHashesForCookie = new HashMap<>();
            servletContext.setAttribute(EMAIL_TO_HASHES_MAP_NAME, emailHashesForCookie);
        }
        // TODO: 5/2/2017 handle collisions of hashed emails
        emailHashesForCookie.put(emailHash, email);
        resp.addCookie(new Cookie(userIDCookieName, emailHash));
    }

    private void removeAnyUserCookies(HttpServletRequest req, HttpServletResponse resp) {
        Optional<Cookie> userIDCookie = Arrays.stream(req.getCookies()).filter(x -> x.getName().equals(userIDCookieName)).findAny();
        if (userIDCookie.isPresent()) {
            Cookie userID = userIDCookie.get();
            userID.setValue(null);
            userID.setMaxAge(0);
            resp.addCookie(userID);
        }
    }

    @SuppressWarnings("unchecked")
    private boolean loginByCookie(HttpServletRequest req) {
        Optional<Cookie> userIDCookie = Arrays.stream(req.getCookies()).filter(x -> x.getName().equals(userIDCookieName)).findAny();
        ServletContext servletContext = req.getServletContext();
        Map<String, String> emailCookieMap = ((Map<String, String>) servletContext.getAttribute(EMAIL_TO_HASHES_MAP_NAME));
        if (userIDCookie.isPresent() && emailCookieMap != null) {
            String emailFromCookie = emailCookieMap.get(userIDCookie.get().getValue());
            Account account = accountService.findAccount(emailFromCookie);
            if (account != null) {
                req.getSession().setAttribute("account", account);
                return true;
            }
            userIDCookie.get().setValue(null);
            userIDCookie.get().setMaxAge(0);
        }
        return false;
    }

}

