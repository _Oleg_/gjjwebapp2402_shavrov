package com.getjavajob.training.web1610.shavrovo.servlets.account;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToAccountPage;
import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToPreviousPage;

/**
 * Created by saul on 4/27/2017.
 */
public class ProfileUpdateServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("Post method in " + getClass().getName() + " invoked");

        HttpSession session = req.getSession();
        Account account = (Account) session.getAttribute("accountToShow");

        String email = req.getParameter("editEmail");
        String firstName = req.getParameter("editFirstName");
        String lastName = req.getParameter("editLastName");
        String middleName = req.getParameter("editMiddleName");
        String password = req.getParameter("editOldPassword");
        String newPassword = req.getParameter("editNewPassword");
        String newPasswordConfirmation = req.getParameter("editNewPasswordConfirmation");
        // TODO: 4/27/2017 handle passwords

        account.setEmail(email);
        account.setFirstName(firstName);
        account.setLastName(lastName);
        account.setMiddleName(middleName);

        AccountService accountService = new AccountService();
        accountService.editAccount(account);
        if (account == req.getSession().getAttribute("account")) {
            req.getSession().setAttribute("account", account);
        }
        resp.sendRedirect(req.getContextPath() + "/accounts/" + account.getId());
    }
}
