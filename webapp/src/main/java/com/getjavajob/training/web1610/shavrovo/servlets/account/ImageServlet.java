package com.getjavajob.training.web1610.shavrovo.servlets.account;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToAccountPage;

/**
 * Created by saul on 4/27/2017.
 */
@WebServlet("/files/*")
@MultipartConfig
public class ImageServlet extends HttpServlet {
    private static String MEDIA_ROOT;

    @Override
    public void init() throws ServletException {
        // set same directory in server.xml too
        MEDIA_ROOT = getServletContext().getInitParameter("MEDIA_ROOT");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String filename = URLDecoder.decode(request.getPathInfo().substring(1), "UTF-8");
        File file = new File(MEDIA_ROOT, filename);
        response.setHeader("Content-Type", getServletContext().getMimeType(filename));
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");
        Files.copy(file.toPath(), response.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Post method in " + getClass().getName() + " invoked");
        Part part = req.getPart("accountAvatarUpload");
        String filename = endcodeUploadedFilename(part.getSubmittedFileName());
        InputStream fileStream = part.getInputStream();
        Files.copy(fileStream, new File(MEDIA_ROOT, filename).toPath());

        AccountService accountService = new AccountService();
        Account accountToShow = (Account) req.getSession().getAttribute("accountToShow");
        accountService.setImage(accountToShow, filename);
        redirectToAccountPage(req, resp);
    }

    private String endcodeUploadedFilename(String filename) {
        // TODO: 5/13/2017 make filename shorter
        String encodedFilename = "accounts/" + System.currentTimeMillis() + filename;
        return encodedFilename;
    }
}
