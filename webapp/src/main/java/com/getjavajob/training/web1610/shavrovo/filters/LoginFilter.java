package com.getjavajob.training.web1610.shavrovo.filters;

import com.getjavajob.training.web1610.shavrovo.Account;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToMainPage;

/**
 * Created by saul on 4/27/2017.
 */
public class LoginFilter implements Filter {
    private static String userIDCookieName;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        userIDCookieName = filterConfig.getServletContext().getInitParameter("userIDCookieName");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        HttpSession session = req.getSession();
        if  (session == null || req.getCookies() == null) {
            redirectToMainPage(req, resp);
        } else if (session.getAttribute("account") == null ) {
            Optional<Cookie> userIDCookie = Arrays.stream(req.getCookies()).filter(x -> x.getName().equals(userIDCookieName)).findAny();
            if (userIDCookie.isPresent()) {
                resp.sendRedirect(req.getContextPath()+"/login");
            } else {
                redirectToMainPage(req, resp);
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    public static String getBaseUrl(HttpServletRequest request) {
        String scheme = request.getScheme() + "://";
        String serverName = request.getServerName();
        String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
        String contextPath = request.getContextPath();
        return scheme + serverName + serverPort + contextPath;
    }

    @Override
    public void destroy() {
    }
}
