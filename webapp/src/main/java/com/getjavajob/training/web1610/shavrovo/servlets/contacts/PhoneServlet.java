package com.getjavajob.training.web1610.shavrovo.servlets.contacts;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.PhoneService;
import com.getjavajob.training.web1610.shavrovo.contacts.Phone;
import com.getjavajob.training.web1610.shavrovo.contacts.PhoneType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToAccountPage;

/**
 * Created by saul on 5/9/2017.
 */
@WebServlet(name = "PhoneServlet", urlPatterns = {"/phones/*"})
public class PhoneServlet extends HttpServlet {
    private PhoneService phoneService;

    @Override
    public void init() throws ServletException {
        phoneService = new PhoneService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("GET from " + getClass().getSimpleName());
        String requestPattern = req.getRequestURI();
        String phoneDeletePath = "/phones/delete";
        if (requestPattern.startsWith(phoneDeletePath)) {
            Pattern phoneId = Pattern.compile("^" + phoneDeletePath + "/(?<phoneId>\\d+)$");
            Matcher phoneIdMatcher = phoneId.matcher(requestPattern);
            if (phoneIdMatcher.matches()) {
                int id = Integer.valueOf(phoneIdMatcher.group("phoneId"));
                Account account = (Account) req.getSession().getAttribute("accountToShow");
                for (Phone phone : account.getPhones()) {
                    if (phone.getId() == id) {
                        phoneService.delete(phone);
                    }
                }
                System.out.println("_____________");
                System.out.println(id);
                System.out.println(requestPattern);
            }
        }
        redirectToAccountPage(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("POST from " + getClass().getSimpleName());
        String requestPattern = req.getRequestURI();
        System.out.println(requestPattern);
        if (requestPattern.startsWith("/phones/edit")) {
            try {
                handleEdit(req);
                handleAdd(req);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        redirectToAccountPage(req, resp);
        System.out.println("END POST from " + getClass().getSimpleName());
    }

    private void handleAdd(HttpServletRequest req) {
        Map<Integer, Map<String, String>> phonesToAdd = getPhoneParameters(req, "phoneAdd");
        for (int id : phonesToAdd.keySet()) {
            String number = phonesToAdd.get(id).get("number");
            PhoneType type = PhoneType.valueOf(phonesToAdd.get(id).get("type"));
            if (number.equals("")) {
                continue;
            }

            Phone phone = new Phone(number, type);
            Account account = (Account) req.getSession().getAttribute("accountToShow");
            account.addPhone(phoneService.addPhoneToAccount(account, phone));
        }
    }

    private void handleEdit(HttpServletRequest req) {
        Map<Integer, Map<String, String>> phonesToEdit = getPhoneParameters(req, "phoneEdit");
        for (int id : phonesToEdit.keySet()) {
            String number = phonesToEdit.get(id).get("number");
            PhoneType type = PhoneType.valueOf(phonesToEdit.get(id).get("type"));

            Phone phone = new Phone(number, type);
            phone.setId(id);
            phoneService.updatePhone(phone);
        }
    }

    private Map<Integer, Map<String, String>> getPhoneParameters(HttpServletRequest req, String prefix) {
        Map<Integer, Map<String, String>> phonesToEdit = new HashMap<>();
        Enumeration<String> allParameters = req.getParameterNames();
        while (allParameters.hasMoreElements()) {
            String param = allParameters.nextElement();
            if (param.startsWith(prefix)) {
                Pattern idPattern = Pattern.compile("\\d+$");
                Pattern phoneIdPattern = Pattern.compile(prefix + "Id\\d+$");
                Pattern phoneNumberPattern = Pattern.compile(prefix + "Number\\d+$");
                Pattern phoneTypePattern = Pattern.compile(prefix + "Type\\d+$");
                Matcher idMatcher = idPattern.matcher(param);
                if (idMatcher.find()) {
                    int id = Integer.valueOf(idMatcher.group());
                    phonesToEdit.putIfAbsent(id, new HashMap<>());
                    if (param.matches(phoneIdPattern.pattern())) {
                        phonesToEdit.get(id).put("id", req.getParameter(param));
                    } else if (param.matches(phoneNumberPattern.pattern())) {
                        phonesToEdit.get(id).put("number", req.getParameter(param));
                    } else if (param.matches(phoneTypePattern.pattern())) {
                        phonesToEdit.get(id).put("type", req.getParameter(param));
                    }
                }
            }
        }
        return phonesToEdit;
    }
}
