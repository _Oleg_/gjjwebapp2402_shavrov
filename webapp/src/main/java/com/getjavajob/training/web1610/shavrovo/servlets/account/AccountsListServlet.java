package com.getjavajob.training.web1610.shavrovo.servlets.account;


import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;
import com.getjavajob.training.web1610.shavrovo.contacts.Contact;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

public class AccountsListServlet extends HttpServlet {
    private String name;
    private AccountService accountService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.name = config.getInitParameter("name");
        accountService = new AccountService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Set<Account> accounts = accountService.getAll();

        PrintWriter writer = resp.getWriter();

        writer.write("<html>");
        writer.write("<head><meta charset=\"UTF-8\"></head>");
        writer.write("<body>");
        writer.write("<ul>");
        for (Account account : accounts) {
            Account fullAccount = accountService.getAccountById(account.getId());
            writer.write("<li>");
            writer.write("<b>");
            writer.write(fullAccount.getId() + " " + fullAccount.getFirstName() + " " + fullAccount.getLastName() + " ");
            writer.write("</b>");
            writer.write("<ol>");
            for (Contact contact : fullAccount.getPhones()) {
                writer.write("<li>" + contact.getContactType() + " " + contact.getLogin() + "</li>");
            }
            writer.write("</ol>");
            writer.write(fullAccount.getHomeAddress() + " ");
            writer.write("</li>");
        }
        writer.write("</ul>");
        writer.write("</body>");
        writer.write("</html>");
    }
}
