package com.getjavajob.training.web1610.shavrovo.servlets.friends;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;
import com.getjavajob.training.web1610.shavrovo.ServiceException;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;
import com.sun.org.apache.xpath.internal.SourceTree;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToAccountPage;

/**
 * Created by saul on 5/6/2017.
 */
@WebServlet(urlPatterns = {"/friends/*", "/friendship/*"})
public class FriendsServlet extends HttpServlet {
    AccountService accountService;

    @Override
    public void init() throws ServletException {
        accountService = new AccountService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("get " + getClass().getName());
        if (req.getRequestURI().equals("/friends")) {
            showMyFriends(req, resp);
        } else if (req.getRequestURI().equals("/friends/requests_sent")) {
            showMyFriendsRequests(req, resp);
        } else if (req.getRequestURI().equals("/friends/requests_received")) {
            showTheirFriendsRequests(req, resp);
        } else {
            redirectToAccountPage(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("POST from " + getClass().getName());

        if (req.getPathInfo().equals("/remove")) {
            removeFromFriends(req, resp);
        } else if (req.getPathInfo().equals("/accept")) {
            acceptFriendshipRequest(req, resp);
        } else if (req.getPathInfo().equals("/decline")) {
            declineFriendshipRequest(req, resp);
        } else if (req.getPathInfo().equals("/send_request")) {
            sendFriendshipRequest(req, resp);
        } else if (req.getPathInfo().equals("/revoke_request")) {
            revokeFriendshipRequest(req, resp);
        }
        redirectToAccountPage(req, resp);
    }

    private void sendFriendshipRequest(HttpServletRequest req, HttpServletResponse resp) {
        String friendShipRequestReceiver = req.getParameter("friendshipRequestReceiver");
        try {
            int receiverId = Integer.valueOf(friendShipRequestReceiver);
            Account sender = (Account) req.getSession().getAttribute("account");
            Account receiver = accountService.getAccountById(receiverId);

            System.out.println("Friendship sender " + sender);
            System.out.println("Friendship receiver " + receiver);

            accountService.sendFriendshipRequest(sender, receiver);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (DAOException e) {
            System.out.println("Given account already in subscribers");
            e.printStackTrace();
        }
    }

    private void acceptFriendshipRequest(HttpServletRequest req, HttpServletResponse resp) {
        String friendshipRequestSender = req.getParameter("acceptFriendshipRequestFrom");
        // TODO: 5/15/2017 handle exceptions by the right way
        try {
            int friendshipRequestSenderID = Integer.valueOf(friendshipRequestSender);
            Account sender = accountService.getAccountById(friendshipRequestSenderID);
            Account receiver = (Account) req.getSession().getAttribute("account");
            accountService.acceptFriendshipRequest(sender, receiver);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void revokeFriendshipRequest(HttpServletRequest req, HttpServletResponse resp) {
        Account sender = (Account) req.getSession().getAttribute("account");
        String friendshipRequestReceiver = req.getParameter("revokeFriendshipRequestTo");
        // TODO: 5/15/2017 handle exceptions the right way
        try {
            int revokedId = Integer.valueOf(friendshipRequestReceiver);
            Account revokedAccount = accountService.getAccountById(revokedId);
            System.out.println("revoked account " + revokedAccount);
            accountService.revokeFriendRequest(sender, revokedAccount);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void declineFriendshipRequest(HttpServletRequest req, HttpServletResponse resp) {
        String friendshipRequestSender = req.getParameter("declineFriendshipRequestFrom");
        try {
            int friendshipSender = Integer.valueOf(friendshipRequestSender);
            Account sender = accountService.getAccountById(friendshipSender);
            Account receiver = (Account) req.getSession().getAttribute("account");
            accountService.declineFriendRequest(sender, receiver);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void removeFromFriends(HttpServletRequest req, HttpServletResponse resp) {
        String removeFromFriends = req.getParameter("removeFromFriends");
        Account currentAccount = (Account) req.getSession().getAttribute("account");
        try {
            Account friendToRemove = accountService.getAccountById(Integer.valueOf(removeFromFriends));
            System.out.println("currentAccount.getFriends() " + currentAccount.getFriends() );
            System.out.println("friendToRemove " + friendToRemove);
            boolean removeStatus = accountService.removeFromFriends(currentAccount, friendToRemove);
            System.out.println("Remove from friends " + removeStatus);
        } catch (ServiceException e) {
            // TODO: 5/15/2017 inform user about it
            e.printStackTrace();
        } catch (NumberFormatException e) {
            // TODO: 5/15/2017 inform user about it
            e.printStackTrace();
        }
    }

    private void showMyFriends(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Account account = (Account) req.getSession().getAttribute("accountToShow");
        System.out.println(account + " has friends: " + account.getFriends());
        Set<Account> friends = new HashSet<>();
        try {
            friends = accountService.getFriendsFor(account);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        req.getSession().setAttribute("account_list", friends);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/relations/friends_list.jsp");
        requestDispatcher.forward(req, resp);
    }


    private void showTheirFriendsRequests(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Account account = (Account) req.getSession().getAttribute("accountToShow");
        if (account != null) {
            Set<Account> theirFriendRequests = accountService.getTheirFriendsPending(account);
            System.out.println(theirFriendRequests);
            req.getSession().setAttribute("account_list", theirFriendRequests);
            req.setAttribute("link_accept", "/friends/requests_received");
            req.setAttribute("link_accept_description", "Add to friends");
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/relations/their_friends_pending_list.jsp");
            requestDispatcher.forward(req, resp);
        } else {
            redirectToAccountPage(req, resp);
        }
    }

    private void showMyFriendsRequests(HttpServletRequest req, HttpServletResponse resp) {
        Account account = (Account) req.getSession().getAttribute("accountToShow");
        System.out.println(account);
        // TODO: 5/15/2017 handle exceptions
        if (account != null) {
            Set<Account> myFriendRequests = accountService.getMyFriendsPending(account);
            System.out.println(myFriendRequests);
            req.getSession().setAttribute("account_list", myFriendRequests);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/relations/my_friends_pending_list.jsp");
            try {
                requestDispatcher.forward(req, resp);
            } catch (ServletException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
