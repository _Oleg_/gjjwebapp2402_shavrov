package com.getjavajob.training.web1610.shavrovo.misc;

import java.util.Collection;

/**
 * Created by saul on 5/15/2017.
 */
public class CheckerContains {
    public static boolean contains(Collection<?> list, Object o) {
        return list.contains(o);
    }
}
