package com.getjavajob.training.web1610.shavrovo.redirects;

import com.getjavajob.training.web1610.shavrovo.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by saul on 5/3/2017.
 */
public class Redirector {
    public static void redirectToMainPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/"));
    }

    public static void redirectToAccountPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Account account = (Account) req.getSession().getAttribute("account");
        if (account != null) {
            resp.sendRedirect(req.getContextPath() + "/accounts/" + account.getId());
        } else {
            throw new IllegalArgumentException("Account is not found");
        }
    }

    public static void redirectToPreviousPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String referrer = req.getHeader("referer");

        System.out.println("referer " + referrer);
//        if (referrer != null) {
            resp.sendRedirect(referrer);
//        } else {
//            throw new IllegalArgumentException("Account is not found");
//        }
    }
}
