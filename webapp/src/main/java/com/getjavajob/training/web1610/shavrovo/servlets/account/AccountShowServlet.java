package com.getjavajob.training.web1610.shavrovo.servlets.account;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;
import com.getjavajob.training.web1610.shavrovo.Permission;
import com.getjavajob.training.web1610.shavrovo.contacts.PhoneType;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by saul on 5/2/2017.
 */
public class AccountShowServlet extends HttpServlet {
    private AccountService accountService;
    private static final int UNEXSIST_ID = -0xDA;

    @Override
    public void init() throws ServletException {
        super.init();
        accountService = new AccountService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println();
        int accountId = UNEXSIST_ID;
        try {
            accountId = Integer.valueOf(req.getPathInfo().substring(1));
            System.out.println(accountId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (accountId != UNEXSIST_ID) {
            Account loggedInAccount = (Account) req.getSession().getAttribute("account");
            Account accountToShow = accountService.getAccountById(accountId);
            System.out.println("Logged in account: " + loggedInAccount);
            System.out.println("Account to show: " + accountToShow);
            if (accountToShow != null) {
                req.getSession().setAttribute("accountToShow", accountToShow);
                req.getSession().setAttribute("phoneTypes", PhoneType.values());

                if (!loggedInAccount.getPermissions().isEmpty()) {
                    Map<String, Boolean> sessionPermissions = new HashMap<>();
                    if (loggedInAccount.hasPermission("edit_account")) {
                        sessionPermissions.put("edit_account", true);
                        System.out.println(sessionPermissions.keySet());
                        req.getSession().setAttribute("permissions", sessionPermissions);
                    }
                }
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/account_info");
                requestDispatcher.forward(req, resp);
            } else {
                System.out.println("Account not exists. Redirect to custom 404 page.");
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/errors/404.jsp");
                resp.setStatus(404);
                requestDispatcher.forward(req, resp);
            }
        }
        System.out.println("doGet from " + getClass().getName());

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost from " + getClass().getName());
    }
}
