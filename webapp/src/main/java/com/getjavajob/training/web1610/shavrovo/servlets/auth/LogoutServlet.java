package com.getjavajob.training.web1610.shavrovo.servlets.auth;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToMainPage;

/**
 * Created by saul on 5/2/2017.
 */
public class LogoutServlet extends HttpServlet {
    private static String userIDCookieName;
    private AccountService accountService;

    @Override
    public void init() throws ServletException {
        super.init();
        userIDCookieName = getServletContext().getInitParameter("userIDCookieName");
        accountService = new AccountService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet from " + getClass().getName());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost from " + getClass().getName());
        for (Cookie c : req.getCookies()) {
            c.setValue(null);
            c.setMaxAge(0);
            resp.addCookie(c);
        }
        redirectToMainPage(req, resp);
    }
}
