package com.getjavajob.training.web1610.shavrovo.servlets.groups;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by saul on 5/9/2017.
 */
@WebServlet(name = "GroupServlet", urlPatterns = {"/groups"})
public class GroupServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("GET from " + getClass().getSimpleName());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/groups_page.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("POST from " + getClass().getSimpleName());
    }
}
