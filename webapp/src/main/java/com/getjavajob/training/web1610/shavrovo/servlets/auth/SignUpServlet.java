package com.getjavajob.training.web1610.shavrovo.servlets.auth;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountBuilder;
import com.getjavajob.training.web1610.shavrovo.AccountService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by saul on 4/24/2017.
 */
public class SignUpServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("regEmail");
        // TODO: 4/27/2017 handle passwords
        String password = req.getParameter("regPassword");
        String passwordConfirmation = req.getParameter("regPasswordConfirmation");
        String firstName = req.getParameter("regFirstName");
        String lastName = req.getParameter("regLastName");
        String middleName = req.getParameter("regMiddleName");

        AccountService accountService = new AccountService();
        AccountBuilder builder = new AccountBuilder();
        Account account = builder.email(email)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
                .createAccount();
        accountService.createAccount(account);
        req.getSession().setAttribute("account", account);
        resp.sendRedirect("/accounts/" + account.getId());
    }
}
