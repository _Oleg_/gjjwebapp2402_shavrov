package com.getjavajob.training.web1610.shavrovo.servlets.friends;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.AccountService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToAccountPage;
import static com.getjavajob.training.web1610.shavrovo.redirects.Redirector.redirectToPreviousPage;

/**
 * Created by saul on 5/15/2017.
 */
@WebServlet(urlPatterns = {"/subscriptions", "/subscriptions/*", "/subscribers"})
public class SubscriptionsServlet extends HttpServlet {
    private AccountService accountService;

    @Override
    public void init() throws ServletException {
        accountService = new AccountService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("GET from " + getClass().getSimpleName());
        if (req.getRequestURI().equals("/subscriptions")) {
            showMySubscriptions(req, resp);
        } else if (req.getRequestURI().equals("/subscribers")) {
            showSubscribers(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("POST from " + getClass().getSimpleName());
        if (req.getPathInfo().equals("/subscribe")) {
            subscribe(req, resp);
        } else if (req.getPathInfo().equals("/unsubscribe/me")) {
            unsubscribeMe(req, resp);
        } else if (req.getPathInfo().equals("/unsubscribe/from_me")) {
            unsubscribeFromMe(req, resp);
        }
    }

    private void subscribe(HttpServletRequest req, HttpServletResponse resp) {
        String subscriptionRequestReceiver = req.getParameter("subscriptionRequestReceiver");
        System.out.println("subscribe ");
        try {
            int subscriptionReceiverId = Integer.valueOf(subscriptionRequestReceiver);
            Account sender = (Account) req.getSession().getAttribute("account");
            Account publisher = accountService.getAccountById(subscriptionReceiverId);
            System.out.println("sender " + sender);
            System.out.println("publisher " + publisher);
            accountService.subscribe(sender, publisher);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        try {
            redirectToAccountPage(req, resp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void unsubscribeMe(HttpServletRequest req, HttpServletResponse resp) {
        String unsubscribeFrom = req.getParameter("unsubscribeFrom");
        unsubscribe(req, unsubscribeFrom);
        try {
            redirectToPreviousPage(req, resp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void unsubscribeFromMe(HttpServletRequest req, HttpServletResponse resp) {
        String subscriberToRemove = req.getParameter("subscriberToRemove");
        unsubscribe(req, subscriberToRemove);
        try {
            redirectToPreviousPage(req, resp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void unsubscribe(HttpServletRequest req, String unsubscribeFrom) {
        try {
            int publisherId = Integer.valueOf(unsubscribeFrom);
            Account sender = (Account) req.getSession().getAttribute("accountToShow");
            Account publisher = accountService.getAccountById(publisherId);
            sender.unsubscribe(publisher);
            accountService.unsubscribe(sender, publisher);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void showMySubscriptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getRequestURI());
        Account account = (Account) req.getSession().getAttribute("accountToShow");
        System.out.println(account);
        System.out.println("from show my subscriptions");
        req.getSession().setAttribute("account_list", account.getSubscriptions());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/relations/my_subscriptions.jsp");
        requestDispatcher.forward(req, resp);
    }

    private void showSubscribers(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getRequestURI());
        Account account = (Account) req.getSession().getAttribute("accountToShow");
        req.getSession().setAttribute("account_list", account.getSubscribers());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/jsp/relations/their_subscriptions.jsp");
        requestDispatcher.forward(req, resp);
    }

}
