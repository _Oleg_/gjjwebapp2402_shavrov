package com.getjavajob.training.web1610.shavrovo.servlets.common;

import com.getjavajob.training.web1610.shavrovo.Account;
import com.getjavajob.training.web1610.shavrovo.Searcher;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * Created by saul on 4/29/2017.
 */
@WebServlet("/search")
public class SearchServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String searchValue = req.getParameter("search_value");
        Searcher searcher = new Searcher();

        Set<Account> searchResults = searcher.getAccountsByNames(searchValue);
        req.setAttribute("searchResults", searchResults);

        System.out.println(searchResults);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF/jsp/common/search_results.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Post from SearchServlet");
    }
}
