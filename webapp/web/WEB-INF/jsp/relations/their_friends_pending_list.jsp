<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:accountpage>
    <ul class="list-group">
        <c:forEach var="item" items="${account_list}">
            <li class="list-group-item">
                <t:listview list_item="${item}"/>
                <hr>
                <form class="float-right" action="${pageContext.request.contextPath}/friendship/accept"
                      method="post">
                    <input type="hidden" name="acceptFriendshipRequestFrom" value="${item.id}">
                    <button type="submit" class="btn">Add to friends</button>
                </form>
                <form class="float-right" action="${pageContext.request.contextPath}/friendship/decline"
                      method="post">
                    <input type="hidden" name="declineFriendshipRequestFrom" value="${item.id}">
                    <button type="submit" class="btn">Left in subscribers</button>
                </form>
            </li>
        </c:forEach>
    </ul>
</t:accountpage>

