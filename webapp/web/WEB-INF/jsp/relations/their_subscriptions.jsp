<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:accountpage>
    <ul class="list-group">
        <c:forEach var="item" items="${account_list}">
            <li class="list-group-item">
                <t:listview list_item="${item}"/>
            </li>
            <form action="${pageContext.request.contextPath}/friendship/requests/accept" method="post">
                <div class="form-group">
                    <input type="hidden" name="addToFriendsFromSubscriptions" value="${item.id}">
                    <button type="submit">Add to friends</button>
                </div>
            </form>
            <form action="${pageContext.request.contextPath}/subscriptions/unsubscribe/from_me" method="post">
                <div class="form-group">
                    <input type="hidden" name="subscriberToRemove" value="${item.id}">
                    <button type="submit">Remove from subscriptions</button>
                </div>
            </form>
        </c:forEach>
    </ul>
</t:accountpage>

