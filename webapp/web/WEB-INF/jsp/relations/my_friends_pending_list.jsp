<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:accountpage>
    <ul class="list-group">
        <c:forEach var="item" items="${account_list}">
            <li class="list-group-item">
                <t:listview list_item="${item}"/>
                <form class="float-right" action="${pageContext.request.contextPath}/friendship/revoke_request"
                      method="post">
                    <div class="form-group">
                        <input type="hidden" name="revokeFriendshipRequestTo" value="${item.id}">
                        <button type="submit" class="btn">Revoke request</button>
                    </div>
                </form>
            </li>
        </c:forEach>
    </ul>
</t:accountpage>

