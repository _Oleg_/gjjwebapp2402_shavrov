<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:accountpage>
    <div>
        <strong>id</strong>
        <p><c:out value="${accountToShow.id}"/></p>
        <strong>email</strong>
        <p><c:out value="${accountToShow.email}"/></p>
        <strong>First name</strong>
        <p><c:out value="${accountToShow.firstName}"/></p>
        <strong>Last name</strong>
        <p><c:out value="${accountToShow.lastName}"/></p>
        <strong>Middle name</strong>
        <p><c:out value="${accountToShow.middleName}"/></p>

        <hr>

        <div class="container">
            <strong>Phones</strong>
            <form class="" action="${pageContext.request.contextPath}/phones/edit" method="post">
                <c:forEach var="phone" items="${accountToShow.phones}">
                    <div class="">
                        <label>${phone.id}</label>
                        <input class="" type="hidden" name="phoneEditId${phone.id}" value="${phone.id}">
                        <input class="" type="tel" name="phoneEditNumber${phone.id}" value="${phone.login}">
                        <select class="" name="phoneEditType${phone.id}">
                            <option
                                    <c:if test="${phone.contactType == phoneTypes[0]}">selected</c:if>
                                    value="${phoneTypes[0]}">${phoneTypes[0]}

                            </option>
                            <option
                                    <c:if test="${phone.contactType == phoneTypes[1]}">selected</c:if>
                                    value="${phoneTypes[1]}">${phoneTypes[1]}
                            </option>
                            <option
                                    <c:if test="${phone.contactType == phoneTypes[2]}">selected</c:if>
                                    value="${phoneTypes[2]}">${phoneTypes[2]}
                            </option>
                            <option
                                    <c:if test="${phone.contactType == phoneTypes[3]}">selected</c:if>
                                    value="${phoneTypes[3]}">${phoneTypes[3]}
                            </option>
                        </select>
                        <a href="${pageContext.request.contextPath}/phones/delete/${phone.id}">x</a>
                </c:forEach>
                <div class="form-group row">
                    <label>*</label>
                    <input type="hidden" name="phoneAddId0" value="0">
                    <input type="tel" name="phoneAddNumber0">
                    <select name="phoneAddType0">
                        <option value="${phoneTypes[0]}">${phoneTypes[0]}</option>
                        <option value="${phoneTypes[1]}">${phoneTypes[1]}</option>
                        <option value="${phoneTypes[2]}">${phoneTypes[2]}</option>
                        <option value="${phoneTypes[3]}">${phoneTypes[3]}</option>
                    </select>
                </div>
                <button type="submit" class="btn">Edit</button>
            </form>
        </div>
    </div>

    <div>
        <hr>
        <strong>Messengers</strong>
        <form class="float-right" action="${pageContext.request.contextPath}/messengers/edit"
              method="post">
            <input type="hidden" name="editMessengers" value="${accountToShow.id}">
            <button type="submit" class="btn">Edit</button>
        </form>
        <br>
        <br>
        <ul>
            <c:forEach var="messenger" items="${accountToShow.messengers}">
                <li>
                    <c:out value="${messenger.login}"/>
                    <c:out value="${messenger.contactType}"/>
                </li>
            </c:forEach>
        </ul>
    </div>

    <div>
        <hr>
        <strong>Groups</strong>
        <form class="float-right" action="${pageContext.request.contextPath}/groups/edit"
              method="post">
            <input type="hidden" name="editGroups" value="${accountToShow.id}">
            <button type="submit" class="btn">Edit</button>
        </form>
        <br>
        <br>
        <ul>
            <c:forEach var="group" items="${accountToShow.groups}">
                <li>
                    <c:out value="${group.name}"/>
                </li>
            </c:forEach>
        </ul>
        <hr>
    </div>
    </div>
</t:accountpage>
