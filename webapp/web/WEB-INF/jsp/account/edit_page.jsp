<%--
  Created by IntelliJ IDEA.
  User: saul
  Date: 4/24/2017
  Time: 12:15 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:genericpage>
    <div class="row">
        <div class="col-lg-3">

        </div>
        <div class="col-lg-6">
            <h3>Profile data</h3>
            <form action="${pageContext.request.contextPath}/update_profile" method="post">
                <div class="form-group">
                    <label for="editEmail">Email</label>
                    <input aria-describedby="emailHelp" class="form-control"
                           id="editEmail" name="editEmail"
                           type="email"
                           value="<c:out value="${sessionScope.accountToShow.email}"/>"
                    >
                    <small class="form-text text-muted" id="emailHelp">We'll never share your email with anyone else.
                    </small>
                </div>
                <div class="form-group">
                    <label for="editOldPassword">Старый пароль</label>
                    <input class="form-control" id="editOldPassword"
                           name="editOldPassword"
                           type="password"
                    >
                </div>
                <div class="form-group">
                    <label for="editNewPassword">Новый пароль</label>
                    <input class="form-control" id="editNewPassword"
                           name="editNewPassword"
                           type="password"
                    >
                </div>
                <div class="form-group">
                    <label for="editNewPasswordConfirmation">Подтверждение пароля</label>
                    <input class="form-control"
                           id="editNewPasswordConfirmation"
                           name="editNewPasswordConfirmation"
                           type="password"
                    >
                </div>
                <div class="form-group">
                    <label for="editFirstName">First name</label>
                    <input class="form-control" id="editFirstName"
                           name="editFirstName"
                           type="text"
                           value="<c:out value="${sessionScope.accountToShow.firstName}"/>"
                    >
                </div>
                <div class="form-group">
                    <label for="editLastName">Last name</label>
                    <input class="form-control" id="editLastName"
                           name="editLastName"
                           type="text"
                           value="<c:out value="${sessionScope.accountToShow.lastName}"/>"
                    >
                </div>
                <div class="form-group">
                    <label for="editMiddleName">Middle name</label>
                    <input class="form-control" id="editMiddleName"
                           name="editMiddleName"
                           type="text"
                           value="<c:out value="${sessionScope.accountToShow.middleName}"/>"
                    >
                </div>
                    <%--<div class="form-group">--%>
                    <%--<label for="regSex">Sex</label><select class="form-control" id="regSex" name="regSex">--%>
                    <%--<option>--%>
                    <%--Male--%>
                    <%--</option>--%>
                    <%--<option>--%>
                    <%--Female--%>
                    <%--</option>--%>
                    <%--<option>--%>
                    <%--Doesn't matter--%>
                    <%--</option>--%>
                    <%--</select>--%>
                    <%--</div>--%>
                    <%--<div class="form-group">--%>
                    <%--<label for="regAboutMe">Say something about you</label>--%>
                    <%--<textarea class="form-control" id="regAboutMe" name="regAboutMe" rows="3"></textarea>--%>
                    <%--</div>--%>
                    <%--<div class="form-group">--%>
                    <%--<label for="regPhotoUpload">Upload your photo</label><input aria-describedby="fileHelp"--%>
                    <%--class="form-control-file"--%>
                    <%--id="regPhotoUpload"--%>
                    <%--name="regPhotoUpload" type="file">--%>
                    <%--<small class="form-text text-muted" id="fileHelp">File with .jpg, .png extension</small>--%>
                    <%--</div>--%>
                    <%--<div class="form-check">--%>
                    <%--<label class="form-check-label"><input class="form-check-input" name="regLicenseAgreement"--%>
                    <%--type="checkbox"> I agree with the <a--%>
                    <%--href="">licence</a> terms</label>--%>
                    <%--</div>--%>
                <button class="btn btn-primary" type="submit">Submit</button>
            </form>
        </div>
        <div class="col-lg-3"></div>
    </div>
</t:genericpage>

