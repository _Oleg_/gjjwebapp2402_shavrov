<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:accountpage>
    <div>
        <a href="${pageContext.request.contextPath}/groups/create">Create group</a>
        <h2>My groups</h2>
    </div>
</t:accountpage>
