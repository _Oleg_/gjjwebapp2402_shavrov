<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
            <h1>404 страница не существует.</h1>
        </div>
        <div class="col-lg-3"></div>
    </div>
</t:genericpage>
