<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>

<c:if test="${empty account}">
    <div>
        <form action="${pageContext.request.contextPath}/login" method="post">
            <div class="form-group">
                <h3>Войти</h3><label for="signInEmail">Email</label><input class="form-control" id="signInEmail"
                                                                           name="signInEmail"
                                                                           placeholder="email@example.com"
                                                                           type="email"
            <c:if test="${not empty sessionScope.wrongInput}">
                                                                           value="${wrongInput["signInEmail"]}"
            </c:if>
            >
            </div>
            <div class="form-group">
                <label for="signInPassword">Password</label><input class="form-control" id="signInPassword"
                                                                   name="signInPassword"
                                                                   placeholder="Password" type="password">
            </div>
            <div class="form-group">
                <label for="signInRememberMe">Запомнить меня</label>
                <input type="checkbox"
                       id="signInRememberMe"
                       name="signInRememberMe"
                <c:if test="${not empty sessionScope.wrongInput['signInRememberMe']}">
                       checked
                </c:if>
                >
            </div>
            <button class="btn btn-primary" type="submit">Вход</button>
        </form>
    </div>
    <br>
    <c:if test="${not empty sessionScope.wrongInput}">
        <div class="alert alert-warning">
            <p>Неправильный логин или пароль</p>
        </div>
    </c:if>
</c:if>
