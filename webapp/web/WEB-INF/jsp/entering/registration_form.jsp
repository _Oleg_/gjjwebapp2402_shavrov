<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>

<h3>Зарегистрироваться</h3>
<form action="${pageContext.request.contextPath}/signUpProcess" method="post">
    <div class="form-group">
        <label for="regEmail">Email</label><input aria-describedby="emailHelp" class="form-control"
                                                  id="regEmail" name="regEmail"
                                                  placeholder="Enter email"
                                                  type="email">
        <small class="form-text text-muted" id="emailHelp">We'll never share your email with anyone else.
        </small>
    </div>
    <div class="form-group">
        <label for="regPassword">Пароль</label><input class="form-control" id="regPassword"
                                                      name="regPassword"
                                                      placeholder="Password" type="password">
    </div>
    <div class="form-group">
        <label for="regPasswordConfirmation">Подтверждение пароля</label><input class="form-control"
                                                                                id="regPasswordConfirmation"
                                                                                name="regPasswordConfirmation"
                                                                                placeholder="Password confirmation"
                                                                                type="password">
    </div>
    <div class="form-group">
        <label for="regFirstName">First name</label><input class="form-control" id="regFirstName"
                                                           name="regFirstName"
                                                           placeholder="First name" type="text">
    </div>
    <div class="form-group">
        <label for="regLastName">Last name</label><input class="form-control" id="regLastName"
                                                         name="regLastName"
                                                         placeholder="Last name" type="text">
    </div>
    <div class="form-group">
        <label for="regMiddleName">Middle name</label><input class="form-control" id="regMiddleName"
                                                             name="regMiddleName"
                                                             placeholder="Middle name" type="text">
    </div>
    <%--<div class="form-group">--%>
    <%--<label for="regSex">Sex</label><select class="form-control" id="regSex" name="regSex">--%>
    <%--<option>--%>
    <%--Male--%>
    <%--</option>--%>
    <%--<option>--%>
    <%--Female--%>
    <%--</option>--%>
    <%--<option>--%>
    <%--Doesn't matter--%>
    <%--</option>--%>
    <%--</select>--%>
    <%--</div>--%>
    <%--<div class="form-group">--%>
    <%--<label for="regAboutMe">Say something about you</label>--%>
    <%--<textarea class="form-control" id="regAboutMe" name="regAboutMe" rows="3"></textarea>--%>
    <%--</div>--%>
    <%--<div class="form-group">--%>
    <%--<label for="regPhotoUpload">Upload your photo</label><input aria-describedby="fileHelp"--%>
    <%--class="form-control-file"--%>
    <%--id="regPhotoUpload"--%>
    <%--name="regPhotoUpload" type="file">--%>
    <%--<small class="form-text text-muted" id="fileHelp">File with .jpg, .png extension</small>--%>
    <%--</div>--%>
    <%--<div class="form-check">--%>
    <%--<label class="form-check-label"><input class="form-check-input" name="regLicenseAgreement"--%>
    <%--type="checkbox"> I agree with the <a--%>
    <%--href="">licence</a> terms</label>--%>
    <%--</div>--%>
    <button class="btn btn-primary" type="submit">Submit</button>
</form>

