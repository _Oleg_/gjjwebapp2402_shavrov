<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <div class="row">
        <div class="col-lg-3">
            <jsp:include page="/WEB-INF/jsp/entering/login_form.jsp"/>
        </div>
        <div class="col-lg-6">
            <button class="btn"><a href="${pageContext.request.contextPath}/registration">Зарегистрироваться</a></button>
        </div>
        <div class="col-lg-3"></div>
    </div>
</t:genericpage>
