<%--
  Created by IntelliJ IDEA.
  User: saul
  Date: 4/24/2017
  Time: 12:15 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <div class="row">
        <div class="col-lg-3">
            <jsp:include page="/WEB-INF/jsp/entering/login_form.jsp"/>
        </div>
        <div class="col-lg-6">
            <jsp:include page="/WEB-INF/jsp/entering/registration_form.jsp"/>
        </div>
        <div class="col-lg-3"></div>
    </div>
</t:genericpage>

