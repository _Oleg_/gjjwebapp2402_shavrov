<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:accountpage>
    <ul class="list-group">
        <c:forEach var="item" items="${searchResults}">
            <li class="list-group-item">
                <t:listview list_item="${item}"/>
            </li>
        </c:forEach>
    </ul>
</t:accountpage>

