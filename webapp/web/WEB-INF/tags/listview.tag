<%@tag description="List view for accounts" pageEncoding="UTF-8" %>
<%@attribute name="list_item" required="true" type="com.getjavajob.training.web1610.shavrovo.Account" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<jsp:include page="/WEB-INF/tags/page_parts/account_item_in_list.jsp">
    <jsp:param name="id" value="${list_item.id}"/>
    <jsp:param name="avatar" value="${list_item.avatarPath}"/>
    <jsp:param name="firstName" value="${list_item.firstName}"/>
    <jsp:param name="lastName" value="${list_item.lastName}"/>
    <jsp:param name="middleName" value="${list_item.middleName}"/>
</jsp:include>
<jsp:doBody/>
