<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>

<form action="${pageContext.request.contextPath}/friendship/send_request" method="post">
    <input type="hidden" name="friendshipRequestReceiver" value="${param.friendshipRequestReceiverId}">
    <button type="submit" class="btn">Send friendship request (1)</button>
</form>

