<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<form action="${pageContext.request.contextPath}/files/upload" method="post"
      enctype="multipart/form-data">
    <input type="file" name="accountAvatarUpload">
    <button type="submit" class="btn">Upload new photo</button>
</form>
