<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>

<a href="${pageContext.request.contextPath}/accounts/${param.id}">
    <img class="img-thumbnail" src="${pageContext.request.contextPath}/files/${param.avatar}" width="100px"
         height="100px">
    <c:out value="${param.firstName}"/>
    "<c:out value="${param.middleName}"/>"
    <c:out value="${param.lastName}"/>
</a>


