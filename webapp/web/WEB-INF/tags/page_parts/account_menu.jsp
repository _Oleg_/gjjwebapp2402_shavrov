<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<ul class="list-group">
    <li class="list-group-item"><a href="${pageContext.request.contextPath}/edit_account">Моя
        <span class="text-muted">(ред)</span></a></li>
    <li class="list-group-item"><a href="${pageContext.request.contextPath}/friends">Мои
        друзья</a></li>
    <li class="list-group-item">
        <a href="${pageContext.request.contextPath}/friends/requests_sent">Мои заявки в
            друзья</a>
    </li>
    <li class="list-group-item">
        <a href="${pageContext.request.contextPath}/friends/requests_received">Полученные заявки
            в друзья</a>
    </li>
    <li class="list-group-item"><a href="${pageContext.request.contextPath}/subscriptions">Мои подписки</a></li>
    <li class="list-group-item"><a href="${pageContext.request.contextPath}/subscribers">Мои подписчики</a>
    </li>
    <li class="list-group-item"><a href="${pageContext.request.contextPath}/groups">Мои группы</a></li>
</ul>
