<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="true" %>
<div class="header clearfix">
    <h3 class="text-muted"><a href="${pageContext.request.contextPath}/">InTouch</a></h3>
    <c:if test="${not empty account}">
        <h4>
            <span>${account.firstName} "${account.middleName}" ${account.lastName}</span>
        </h4>
        <nav>
            <ul class="nav nav-pills float-left">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/">Home</a>
                </li>
                <li class="nav-item">
                    <!--todo highlight current page in navigation use active attribute and <span class="sr-only">(current)</span> in nav-->
                    <a class="nav-link" href="${pageContext.request.contextPath}/accounts/${account.id}">Моя
                        страница</a>
                </li>
                <form action="${pageContext.request.contextPath}/logout" method="post">
                    <button class="btn btn-default" type="submit">Logout</button>
                </form>
            </ul>
            <div class="col-sm-3 col-md-3 float-right">
                <form class="navbar-form" role="search" action="${pageContext.request.contextPath}/search"
                      method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="search_value">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </nav>
    </c:if>
</div>
