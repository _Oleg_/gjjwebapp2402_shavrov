<%@tag description="Overall page template" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html class="no-js">
<%@include file="page_parts/header.html" %>
<body>
<!--[if IE]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->

<div class="container">
    <jsp:include page="/WEB-INF/tags/page_parts/navigation.jsp"/>
    <jsp:doBody/>
</div>

<%@include file="page_parts/js_common_usage.html" %>
</body>
</html>