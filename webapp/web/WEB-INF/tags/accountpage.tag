<%@tag description="Overall page template" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="/WEB-INF/tld/customTagLibrary" prefix="fn" %>

<t:genericpage>
    <div class="row">
        <div class="col-lg-3">
            <div class="container-fluid">
                <img class="img-thumbnail"
                <c:choose>
                <c:when test="${not empty accountToShow.avatarPath}">
                     src="${pageContext.request.contextPath}/files/${accountToShow.avatarPath}">
                </c:when>
                <c:otherwise>
                    src="${pageContext.request.contextPath}/files/default/a.jpg">
                </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${accountToShow.id != account.id }">
                        <c:if test="${sessionScope.permissions['edit_account']}">
                            <jsp:include page="/WEB-INF/tags/page_parts/image_change.jsp"/>
                            <br>
                        </c:if>

                        <c:if test="${
                        not fn:contains(sessionScope.account.friends, sessionScope.accountToShow) and
                        not fn:contains(sessionScope.account.myFriendsPending, sessionScope.accountToShow)
                        }">
                            <form action="${pageContext.request.contextPath}/friendship/send_request" method="post">
                                <input type="hidden" name="friendshipRequestReceiver"
                                       value="${accountToShow.id}">
                                <button type="submit" class="btn">Send friendship request</button>
                            </form>
                        </c:if>

                        <hr>
                        <c:if test="${not fn:contains(sessionScope.accountToShow.subscribers, sessionScope.account)}">
                            <form action="${pageContext.request.contextPath}/subscriptions/subscribe" method="post">
                                <input type="hidden" name="subscriptionRequestReceiver" value="${accountToShow.id}">
                                <button type="submit" class="btn">Subscribe</button>
                            </form>
                        </c:if>
                        <c:if test="${sessionScope.permissions['edit_account']}">
                            <jsp:include page="/WEB-INF/tags/page_parts/account_menu.jsp"/>
                            <br>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <br><br>
                        <jsp:include page="/WEB-INF/tags/page_parts/image_change.jsp"/>
                        <br>
                        <jsp:include page="/WEB-INF/tags/page_parts/account_menu.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="col-lg-6">
            <jsp:doBody/>
        </div>
        <div class="col-lg-3"></div>
    </div>
</t:genericpage>
