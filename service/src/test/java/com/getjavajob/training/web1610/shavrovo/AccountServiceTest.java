package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.contacts.*;
import com.getjavajob.training.web1610.shavrovo.dao.*;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.MessengerDAO;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.PermissionDAO;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.PhoneDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import com.getjavajob.training.web1610.shavrovo.dao.relations.AccountRelationDAOWrapper;
import com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by saul on 29.03.2017.
 */
class AccountServiceTest {
    private Account account;
    private Account friend1;
    private Account friend2;
    private Account friend3;
    private Account subscriber1;
    private Account subscriber2;
    private Account subscriber3;
    private Account subscription1;
    private Account subscription2;
    private Account subscription3;

    private Phone personalPhone;
    private Phone workPhone;
    private Messenger telegramMessenger;

    private EntityDAO<Account> accountDAO = mock(AccountDAO.class);
    private PhoneDAO phoneDAO = mock(PhoneDAO.class);
    private MessengerDAO messengerDAO = mock(MessengerDAO.class);
    private GroupDAO groupDAO = mock(GroupDAO.class);
    private PermissionDAO permissionDAO = mock(PermissionDAO.class);
    private AccountRelationDAOWrapper relationDAO = mock(AccountRelationDAOWrapper.class);
    private ConnectionPool connectionPool = mock(ConnectionPool.class);
    private AccountService accountService;

    @BeforeEach
    void createAccounts() throws Exception {
        account = new AccountBuilder().id(111).email("account@mail.ru").password("password").createAccount();
        friend1 = new AccountBuilder().id(0x1F).email("friend1@mail.ru").password("password").createAccount();
        friend2 = new AccountBuilder().id(0x2F).email("friend2@mail.ru").password("password").createAccount();
        friend3 = new AccountBuilder().id(0x3F).email("friend3@mail.ru").password("password").createAccount();
        subscriber1 = new AccountBuilder().id(0x1C).email("subscriber1@mail.ru").password("password").createAccount();
        subscriber2 = new AccountBuilder().id(0x2C).email("subscriber2@mail.ru").password("password").createAccount();
        subscriber3 = new AccountBuilder().id(0x3C).email("subscriber3@mail.ru").password("password").createAccount();
        subscription1 = new AccountBuilder().id(0x1CA).email("subscription1@mail.ru").password("password").createAccount();
        subscription2 = new AccountBuilder().id(0x2CA).email("subscription2@mail.ru").password("password").createAccount();
        subscription3 = new AccountBuilder().id(0x3CA).email("subscription3@mail.ru").password("password").createAccount();

        account.setFriends(new HashSet<>(Arrays.asList(friend1, friend2, friend3)));
        account.setSubscribers(new HashSet<>(Arrays.asList(subscriber1, subscriber2, subscriber3)));
        account.setSubscriptions(new HashSet<>(Arrays.asList(subscription1, subscription2, subscription3)));

        personalPhone = new Phone("89244547515", PhoneType.PERSONAL);
        workPhone = new Phone("89244547515", PhoneType.WORK);
        telegramMessenger = new Messenger("89244547515", MessengerType.TELEGRAM);

        account.setPhones(new HashSet<>(Arrays.asList(personalPhone, workPhone)));
        account.setMessengers(new HashSet<>(Collections.singleton(telegramMessenger)));

        when(permissionDAO.getAllBounded(account)).thenReturn(null);
        doNothing().when(relationDAO).createAllRelatedFrom(account, RelationshipDAO.RelationStatus.FRIEND);
        doNothing().when(relationDAO).createAllRelatedFrom(account, RelationshipDAO.RelationStatus.FRIEND_PENDING);
        when(relationDAO.getAllRelatedFrom(account, RelationshipDAO.RelationStatus.FRIEND)).thenReturn(account.getFriends());
        when(relationDAO.getAllRelatedFrom(account, RelationshipDAO.RelationStatus.FRIEND_PENDING)).thenReturn(account.getFriends());

        when(connectionPool.acquireConnection()).thenReturn(null);
        doNothing().when(connectionPool).commit();
        doNothing().when(connectionPool).rollback();
        doNothing().when(connectionPool).releaseConnection();
    }

    @Test
    void createAccountsWithPhones() throws Exception {
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);

        AccountBuilder builder;
        int testCount = 0;

        String password = "password1";
        Set<Phone> phones = new HashSet<>();
        phones.add(new Phone("89999999999", PhoneType.HOME));
        phones.add(new Phone("85555555555", PhoneType.WORK));

        builder = new AccountBuilder();
        String email1 = "email" + testCount++ + "@mail.com";
        Account account1 = builder.email(email1).password(password).firstName("John").lastName("Doe").phones(phones).homeAddress("Home address").createAccount();
        account1.setId(1);
        when(accountDAO.create(account1)).thenReturn(account1);
        accountService.createAccount(account1);

        builder = new AccountBuilder();
        String email2 = "email" + testCount++ + "@mail.com";
        Account account2 = builder.email(email2).password(password).firstName("Vanya").lastName("Ivanov").phones(phones).homeAddress("Home address").createAccount();
        account2.setId(2);
        when(accountDAO.create(account2)).thenReturn(account2);
        accountService.createAccount(account2);

        builder = new AccountBuilder();
        String email3 = "email" + testCount++ + "@mail.com";
        Account account3 = builder.email(email3).password(password).firstName("Igor").lastName("Sidorov").phones(phones).homeAddress("Home address").createAccount();
        account3.setId(3);
        when(accountDAO.create(account3)).thenReturn(account3);
        accountService.createAccount(account3);

        builder = new AccountBuilder();
        String email4 = "email" + testCount++ + "@mail.com";
        Account account4 = builder.email(email4).password(password).firstName("Ilya").lastName("Murometz").phones(phones).homeAddress("Home address").createAccount();
        account4.setId(4);
        when(accountDAO.create(account4)).thenReturn(account4);
        accountService.createAccount(account4);

        builder = new AccountBuilder();
        String email5 = "email" + testCount++ + "@mail.com";
        Account account5 = builder.email(email5).password(password).firstName("Aleosha").lastName("Popovich").phones(phones).homeAddress("Home address").createAccount();
        account5.setId(5);
        when(accountDAO.create(account5)).thenReturn(account5);
        accountService.createAccount(account5);

        accountDAO.getAll();
        Set<Account> accounts = accountDAO.getAll();
        for (Account account : accounts) {
            Account fullAccount = accountService.getAccountById(account.getId());
            assertThat(account.getId(), equalTo(fullAccount.getId()));
            assertThat(account.getEmail(), equalTo(fullAccount.getEmail()));
            assertThat(account.getFirstName(), equalTo(fullAccount.getFirstName()));
            assertThat(account.getPhones(), equalTo(fullAccount.getPhones()));
        }
    }

    @Test
    void createAccount() throws Exception {
        when(accountDAO.create(account)).thenReturn(account);
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);

        Account createdAccount = accountService.createAccount(account);
        assertThat(createdAccount.getFriends(), equalTo(account.getFriends()));
        assertEquals(createdAccount.getId(), account.getId());
    }

    @Test
    void editAccount() throws Exception {
        when(accountDAO.create(account)).thenReturn(account);
        when(accountDAO.update(account)).thenReturn(account);

        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);

        Account createdAccount = accountService.createAccount(account);
        String newEmail = "new_email@mail.ru";
        createdAccount.setEmail(newEmail);
        Account updatedAccount = accountService.editAccount(createdAccount);

        assertThat(updatedAccount.getFriends(), equalTo(account.getFriends()));
        assertEquals(updatedAccount.getId(), account.getId());
        assertThat(updatedAccount.getEmail(), equalTo(newEmail));
    }

    @Test
    void getAccountById() throws Exception {
        when(accountDAO.getById(account.getId())).thenReturn(account);
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);

        Account gotAccount = accountService.getAccountById(account.getId());
        assertThat(gotAccount.getFriends(), equalTo(account.getFriends()));
        assertEquals(gotAccount.getId(), account.getId());
        assertThat(gotAccount.getEmail(), equalTo(account.getEmail()));
    }

    @Test
    void deleteAccount() throws Exception {
        when(accountDAO.create(account)).thenReturn(account);
        when(accountDAO.getById(account.getId())).thenReturn(account);
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);

        Account createdAccount = accountService.createAccount(account);
        Account gotAccount = accountService.getAccountById(createdAccount.getId());

        accountService.deleteAccount(gotAccount);
        verify(accountDAO).delete(gotAccount);
    }

    @Test
    void sendFriendRequest() throws Exception {
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);
        accountService.sendFriendshipRequest(account, friend1);
        assertThat(account, in(friend1.getTheirFriendsPending()));
    }

    @Test
    void acceptFriendshipRequest() throws Exception {
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);
        Account from = account;
        Account to = friend1;
        accountService.sendFriendshipRequest(from, to);
        assertThat(from, in(to.getTheirFriendsPending()));
        accountService.acceptFriendshipRequest(from, to);
        assertThat(from, in(to.getFriends()));
    }

    @Test
    void declineFriendRequest() throws Exception {
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);
        Account from = account;
        Account to = friend1;
        accountService.sendFriendshipRequest(from, to);
        assertThat(from, in(to.getTheirFriendsPending()));
        accountService.declineFriendRequest(from, to);
        assertThat(from, not(in(to.getFriends())));
    }

    @Test
    void friendsAndSubscribers() {
        AccountService accountService = new AccountService(accountDAO, phoneDAO, messengerDAO, relationDAO, groupDAO, permissionDAO, connectionPool);
        Set<Account> friendsAndSubscribers = accountService.friendsAndSubscribers(account);
        Set<Account> fs = new HashSet<>(account.getFriends());
        fs.addAll(account.getSubscribers());

        assertThat(fs, equalTo(friendsAndSubscribers));
    }

}