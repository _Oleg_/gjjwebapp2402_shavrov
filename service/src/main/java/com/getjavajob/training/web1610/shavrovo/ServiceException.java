package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.dao.DAOException;

/**
 * Created by saul on 5/15/2017.
 */
public class ServiceException extends Throwable {
    public ServiceException(Exception e) {
        super(e);
    }
}
