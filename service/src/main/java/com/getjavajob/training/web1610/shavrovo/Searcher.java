package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;

import java.util.HashSet;
import java.util.Set;


/**
 * Created by saul on 4/29/2017.
 */
public class Searcher {
    private EntityDAO<Account> accountDAO;

    public Searcher() {
        accountDAO = new AccountDAO();
    }

    public Set<Account> getAccountsByNames(String name) {
        Set<Account> accounts = new HashSet<>();
        try {
            accounts.addAll(accountDAO.getAllByKeyValuePair("firstName", name));
            accounts.addAll(accountDAO.getAllByKeyValuePair("lastName", name));
            accounts.addAll(accountDAO.getAllByKeyValuePair("middleName", name));
            return accounts;
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
