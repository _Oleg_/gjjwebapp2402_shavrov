package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.contacts.Messenger;
import com.getjavajob.training.web1610.shavrovo.contacts.Phone;
import com.getjavajob.training.web1610.shavrovo.dao.*;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.MessengerDAO;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.PermissionDAO;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.PhoneDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.AccountDAO;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import com.getjavajob.training.web1610.shavrovo.dao.relations.AccountRelationDAOWrapper;
import com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static com.getjavajob.training.web1610.shavrovo.DBInitializer.COMMON_PROPERTIES;
import static com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO.RelationStatus.FRIEND;
import static com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO.RelationStatus.FRIEND_PENDING;
import static com.getjavajob.training.web1610.shavrovo.dao.relations.RelationshipDAO.RelationStatus.SUBSCRIBER;

/**
 * Created by saul on 27.03.2017.
 */
@AllArgsConstructor
@Builder
public class AccountService {
    private EntityDAO<Account> accountDAO;
    private PhoneDAO phoneDAO;
    private MessengerDAO messengerDAO;
    private AccountRelationDAOWrapper relationDAO;
    private GroupDAO groupDAO;
    private PermissionDAO permissionDAO;

    private ConnectionPool connectionPool;

    public AccountService() {
        accountDAO = new AccountDAO();
        phoneDAO = new PhoneDAO();
        messengerDAO = new MessengerDAO();
        relationDAO = new AccountRelationDAOWrapper();
        groupDAO = new GroupDAO();
        permissionDAO = new PermissionDAO();
        connectionPool = ConnectionPool.getInstance();
    }

    public Set<Account> getAll() {
        try {
            return accountDAO.getAll();
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Account createAccount(Account account) {
        try {
            int id = accountDAO.create(account).getId();
            account.setId(id);
            for (Phone phone : account.getPhones()) {
                phoneDAO.createRelation(account, phone);
            }
            for (Messenger messenger : account.getMessengers()) {
                phoneDAO.createRelation(account, messenger);
            }
            for (Permission permission : account.getPermissions()) {
                permissionDAO.createRelation(account, permission);
            }
            saveRelations(account);

            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
        return account;
    }

    private void saveRelations(Account account) throws DAOException {
        relationDAO.createAllRelatedTo(account, FRIEND); // save friends
        relationDAO.createAllRelatedFrom(account, FRIEND_PENDING); // save myFriendsPending
        relationDAO.createAllRelatedTo(account, FRIEND_PENDING); // save theirFriendsPending
        relationDAO.createAllRelatedFrom(account, RelationshipDAO.RelationStatus.SUBSCRIBER); // save subscriptions
        relationDAO.createAllRelatedTo(account, RelationshipDAO.RelationStatus.SUBSCRIBER); // save subscribers
    }

    public Account editAccount(Account account) {
        Account editedAccount = null;
        try {
            editedAccount = accountDAO.update(account);
            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
        return editedAccount;
    }

    public Account getAccountById(int id) {
        Account fromDB = null;
        try {
            fromDB = accountDAO.getById(id);
            if (fromDB != null) {
                fromDB.setPhones(phoneDAO.getAllBounded(fromDB));
                fromDB.setMessengers(messengerDAO.getAllBounded(fromDB));
                fromDB.setPermissions(permissionDAO.getAllBounded(fromDB));
//                fromDB.setGroups(groupDAO.getAllBounded(fromDB));

                fromDB.setFriends(relationDAO.getAllRelatedFrom(fromDB, FRIEND));
                fromDB.setMyFriendsPending(relationDAO.getAllRelatedFrom(fromDB, FRIEND_PENDING));
                fromDB.setTheirFriendsPending(relationDAO.getAllRelatedTo(fromDB, FRIEND_PENDING));
                fromDB.setSubscriptions(relationDAO.getAllRelatedFrom(fromDB, SUBSCRIBER));
                fromDB.setSubscribers(relationDAO.getAllRelatedTo(fromDB, SUBSCRIBER));
            }
            // TODO: 5/13/2017 throw exceptions to inform user about error
            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
        return fromDB;
    }

    public boolean deleteAccount(Account account) {
        try {
            boolean deleteStatus = accountDAO.delete(account);
            connectionPool.commit();
            return deleteStatus;
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
        return false;
    }

    public void sendFriendshipRequest(Account sender, Account receiver) throws DAOException {
        sender.sendFriendshipRequest(receiver);
        sender.subscribe(receiver);
        try {
            relationDAO.createRelation(sender, receiver, SUBSCRIBER);
            relationDAO.createRelation(sender, receiver, FRIEND_PENDING);
            connectionPool.commit();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
    }

    public void acceptFriendshipRequest(Account from, Account to) {
        to.acceptFriendRequest(from);
        try {
            relationDAO.createRelation(to, from, FRIEND);
            relationDAO.createRelation(from, to, FRIEND);
            relationDAO.deleteRelation(from, to, FRIEND_PENDING);
            relationDAO.deleteRelation(to, from, FRIEND_PENDING);
            relationDAO.deleteRelation(from, to, SUBSCRIBER);
            relationDAO.deleteRelation(to, from, SUBSCRIBER);
            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
    }

    public void declineFriendRequest(Account sender, Account receiver) {
        receiver.declineFriendshipRequest(sender);
        try {
            relationDAO.deleteRelation(sender, receiver, FRIEND_PENDING);
            relationDAO.deleteRelation(receiver, sender, FRIEND_PENDING);
            relationDAO.createRelation(sender, receiver, SUBSCRIBER);
            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
    }

    public Set<Account> friendsAndSubscribers(Account account) {
        Set<Account> friendsAndSubscribers = new HashSet<>();
        friendsAndSubscribers.addAll(account.getFriends());
        friendsAndSubscribers.addAll(account.getSubscribers());
        return friendsAndSubscribers;
    }

    public Account findAccount(String email) {
        try {
            Account account = accountDAO.getEntityByUniqueKey(email);
            connectionPool.commit();
            if (account != null) {
                return getAccountById(account.getId());
            }
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
        return null;
    }

    public Account findAccount(String email, String password) {
        try {
            Account account = accountDAO.getEntityByUniqueKey(email);
            if (account != null && account.getPassword().equals(password)) {
                Account foundAccount = getAccountById(account.getId());
                return foundAccount;
            }
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean removeFromFriends(Account who, Account whom) throws ServiceException {
        boolean removeResult = who.removeFriend(whom);
        try {
            relationDAO.deleteRelation(who, whom, FRIEND);
            relationDAO.deleteRelation(whom, who, FRIEND);
            relationDAO.createRelation(whom, who, SUBSCRIBER);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        ConnectionPool.getInstance().commit();
        ConnectionPool.getInstance().releaseConnection();
        return removeResult;
    }

    public Set<Account> getMyFriendsPending(Account account) {
        try {
            Set<Account> myFriendsPending = relationDAO.getAllRelatedFrom(account, FRIEND_PENDING);
            connectionPool.commit();
            return myFriendsPending;
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
        return null;
    }

    public Set<Account> getTheirFriendsPending(Account account) {
        try {
            Set<Account> theirFriendsPending = relationDAO.getAllRelatedTo(account, FRIEND_PENDING);
            connectionPool.commit();
            return theirFriendsPending;
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
        return null;
    }

    public String getSecret() {
        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(COMMON_PROPERTIES);
        Properties properties = new Properties();
        try {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return properties.getProperty("secret");
    }

    public void subscribe(Account sender, Account receiver) {
        if (sender.equals(receiver)) {
            return;
        }
        sender.subscribe(receiver);
        try {
            relationDAO.createRelation(sender, receiver, SUBSCRIBER);
            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
    }

    public void unsubscribe(Account subscriber, Account publisher) {
        if (publisher.equals(subscriber)) {
            return;
        }
        subscriber.unsubscribe(publisher);
        try {
            relationDAO.deleteRelation(subscriber, publisher, SUBSCRIBER);
            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
    }

    public void revokeFriendRequest(Account sender, Account receiver) {
        sender.getMyFriendsPending().remove(receiver);
        receiver.getTheirFriendsPending().remove(sender);
        try {
            relationDAO.deleteRelation(receiver, sender, FRIEND_PENDING);
            relationDAO.deleteRelation(sender, receiver, FRIEND_PENDING);
            connectionPool.commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            connectionPool.rollback();
            connectionPool.releaseConnection();
        }
    }

    public void setImage(Account accountToShow, String filename) {
        accountToShow.setAvatarPath(filename);
        try {
            accountDAO.update(accountToShow);
            ConnectionPool.getInstance().commit();
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
    }

    public Set<Account> getFriendsFor(Account account) throws DAOException {
        return relationDAO.getAllRelatedTo(account, FRIEND);
    }


}
