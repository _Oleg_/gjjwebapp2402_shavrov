package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.dao.DAOException;
import com.getjavajob.training.web1610.shavrovo.dao.entity.EntityDAO;
import com.getjavajob.training.web1610.shavrovo.dao.GroupDAO;

/**
 * Created by saul on 4/26/2017.
 */
public class GroupService {
    private EntityDAO<Group> groupEntityDAO = new GroupDAO();

    public Group createGroup(Group group) {
        try {
            return groupEntityDAO.create(group);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Group updateGroup(Group group) {
        try {
            return groupEntityDAO.update(group);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Group getGroupById(int id) {
        try {
            return groupEntityDAO.getById(id);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean deleteGroup(Group group) {
        try {
            return groupEntityDAO.delete(group);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void assignAdmin(Account admin, Group group) {
        group.assignAdmin(admin);
    }

}
