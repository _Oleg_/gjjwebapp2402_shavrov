package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.contacts.Phone;
import com.getjavajob.training.web1610.shavrovo.dao.DAOException;
import com.getjavajob.training.web1610.shavrovo.dao.bounded.PhoneDAO;

/**
 * Created by saul on 5/11/2017.
 */
public class PhoneService {
    private PhoneDAO phoneDAO;

    public PhoneService() {
        phoneDAO = new PhoneDAO();
    }

    public Phone updatePhone(Phone phone) {
        try {
            Phone updated = phoneDAO.update(phone);
            ConnectionPool.getInstance().commit();
            return updated;
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
        return null;
    }

    public Phone addPhoneToAccount(Account account, Phone phone) {
        try {
            Phone created = phoneDAO.create(phone);
            phoneDAO.createRelation(account, created);
            ConnectionPool.getInstance().commit();
            return created;
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
        return null;
    }

    public boolean delete(Phone phone) {
        try {
            boolean deleteResult = phoneDAO.delete(phone);
            ConnectionPool.getInstance().commit();
            return deleteResult;
        } catch (DAOException e) {
            e.printStackTrace();
        } finally {
            ConnectionPool.getInstance().rollback();
            ConnectionPool.getInstance().releaseConnection();
        }
        return false;
    }
}
