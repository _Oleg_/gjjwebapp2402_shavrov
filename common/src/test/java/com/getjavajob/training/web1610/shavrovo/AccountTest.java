package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.contacts.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by saul on 12.03.2017.
 */
public class AccountTest {
    private int id1 = 92;
    private String email1 = "email@maaiil.com";
    private String password1 = "paosijfowi";
    private Account account;

    private int id2 = 39;
    private String email2 = "subs@mail.com";
    private String password2 = "powiejf";
    private Account publisher;

    @BeforeEach
    void createAccounts() {
        account = new AccountBuilder().id(id1).email(email1).password(password1).createAccount();
        publisher = new AccountBuilder().id(id2).email(email2).password(password2).createAccount();
    }

    @AfterEach
    void cleanUpAccounts() {
        account = null;
        publisher = null;
    }

    @Test
    void addSubscription() {
        account.subscribe(publisher);
        assertThat(account.getSubscriptions(), contains(publisher));
        assertThat(publisher.getSubscribers(), contains(account));
    }

    @Test
    void unsubscribe() {
        account.subscribe(publisher);
        assertThat(publisher.getSubscribers(), contains(account));
        account.unsubscribe(publisher);
        assertThat(publisher.getSubscribers(), not(contains(account)));
    }

    @Test
    void sendFriendRequest() {
        account.sendFriendshipRequest(publisher);
        assertThat(publisher.getSubscribers(), contains(account));
        assertThat(account.getSubscriptions(), contains(publisher));
        assertThat(publisher.getFriends(), not(contains(account)));
        assertThat(account.getFriends(), not(contains(publisher)));
    }

    @Test
    void acceptFriendRequest() {
        account.sendFriendshipRequest(publisher);
        publisher.acceptFriendRequest(account);
        assertThat(account, in(publisher.getFriends()));
        assertThat(publisher, in(account.getFriends()));
        assertThat(account, not(in(publisher.getSubscribers())));
        assertThat(publisher, not(in(account.getSubscribers())));
    }

    @Test
    void declineFriendRequest() {
        account.sendFriendshipRequest(publisher);
        publisher.declineFriendshipRequest(account);
        assertThat(account.getFriends(), not(contains(publisher)));
        assertThat(publisher.getFriends(), not(contains(account)));
        assertThat(account.getSubscribers(), not(contains(publisher)));
        assertThat(account.getSubscriptions(), contains(publisher));
        assertThat(publisher.getSubscribers(), contains(account));
    }

    @Test
    void addFriend() {
        account.addFriend(publisher);
        publisher.addFriend(account);
        assertThat(account.getFriends(), equalTo(new HashSet<>(Arrays.asList(publisher))));
        assertThat(publisher.getFriends(), equalTo(new HashSet<>(Arrays.asList(account))));
    }

    @Test
    void removeFriend() {
        account.sendFriendshipRequest(publisher);
        publisher.acceptFriendRequest(account);
        publisher.removeFriend(account);

        assertThat(publisher.getFriends(), not(contains(account)));
        assertThat(account.getFriends(), not(contains(publisher)));
        assertThat(publisher.getSubscribers(), contains(account));
        assertThat(account.getSubscriptions(), contains(publisher));
    }

    @Test
    void addPhones() {
        String personalPhone = "89234445555";
        Phone personalPhoneContact = new Phone(personalPhone, PhoneType.PERSONAL);
        Phone workPhoneContact = new Phone("89234445556", PhoneType.WORK);
        Phone additionalPhoneContact = new Phone("89234445557", PhoneType.ADDITIONAL);

        Set<Phone> phones = new HashSet<>(Arrays.asList(personalPhoneContact, workPhoneContact, additionalPhoneContact));
        account.setPhones(phones);

        assertThat(personalPhoneContact, in(account.getPhones()));
        assertThat(workPhoneContact, in(account.getPhones()));
        assertThat(additionalPhoneContact, in(account.getPhones()));
    }

    @Test
    void addMessengers() {
        String messenger = "89234445555";
        Set<Messenger> messengers = new HashSet<>();
        Messenger contact = new Messenger(messenger, MessengerType.TELEGRAM);
        messengers.add(contact);
        account.setMessengers(messengers);

        assertTrue(account.getMessengers().contains(contact));
    }
}
