package com.getjavajob.training.web1610.shavrovo.contacts;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by saul on 14.04.2017.
 */
class PhoneTest {

    @Test
    void phoneCreationTest() {
        String phoneNumber = "89224445555";
        ContactType phoneType = PhoneType.HOME;
        Phone phone = new Phone(phoneNumber, phoneType);
        assertThat(phone.getContactType(), equalTo(phone.getContactType()));
        assertThat(phone.getContactType().toString(), equalTo(phone.getContactType().toString()));
    }
}