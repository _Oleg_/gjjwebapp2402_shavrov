package com.getjavajob.training.web1610.shavrovo;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsIn.in;

/**
 * Created by saul on 15.03.2017.
 */
class GroupTest {
    @Test
    void getAdmins() {
        Account member1 = new AccountBuilder().id(30).email("email@mail.com").password("password").createAccount();
        Account member2 = new AccountBuilder().id(238).email("email@mail.ro").password("accPassword1").createAccount();
        Account member3 = new AccountBuilder().id(32).email("accEmail@gmail.com").password("accPassword1").createAccount();
        Account admin = new AccountBuilder().id(29).email("acc@Email.com").password("qwerty").createAccount();

        Group group = new GroupBuilder( "Awesome group").createGroup();
        group.addMember(member1);
        group.addMember(member2);
        group.addMember(member3);
        group.addMember(admin);
        group.assignAdmin(admin);
        assertThat(admin, in(group.getAdmins()));
    }

    @Test
    void getMembers() {
        Account member1 = new AccountBuilder().id(30).email("email@mail.com").password("password").createAccount();
        Account member2 = new AccountBuilder().id(238).email("email@mail.ro").password("accPassword1").createAccount();
        Account member3 = new AccountBuilder().id(32).email("accEmail@gmail.com").password("accPassword1").createAccount();
        Account admin1 = new AccountBuilder().id(29).email("acc@Email.com").password("qwerty").createAccount();

        Group group = new GroupBuilder("Awesome group").createGroup();
        group.assignAdmin(admin1);
        group.addMember(member1);
        group.addMember(member2);
        group.addMember(member3);
        assertThat(group.getMembers(), containsInAnyOrder(member1, member2, member3));
    }
}