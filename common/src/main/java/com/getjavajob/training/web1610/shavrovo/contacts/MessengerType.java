package com.getjavajob.training.web1610.shavrovo.contacts;

/**
 * Created by saul on 14.04.2017.
 */
public enum MessengerType implements ContactType {
    SKYPE, TELEGRAM, WHATSAPP, VIBER;

    @Override
    public String contactType() {
        return this.name();
    }

    public static ContactType findEnum(String contactType) {
        for (MessengerType type : MessengerType.values()) {
            if (contactType.equals(type.name())) {
                return type;
            }
        }
        return null;
    }
}
