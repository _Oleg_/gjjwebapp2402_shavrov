package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.contacts.Messenger;
import com.getjavajob.training.web1610.shavrovo.contacts.Phone;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by saul on 12.03.2017.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private int id;
    private Timestamp regTimestamp;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String homeAddress;
    private String workAddress;
    private String avatarPath;

    private Set<Phone> phones;
    private Set<Messenger> messengers;

    private Set<Account> friends;
    private Set<Account> myFriendsPending;
    private Set<Account> theirFriendsPending;
    private Set<Account> subscribers;
    private Set<Account> subscriptions;
    private Set<Group> groups;
    private Set<Permission> permissions;


    public boolean addPhone(Phone phone) {
        return phones.add(phone);
    }

    public boolean removePhone(Phone phone) {
        return phones.remove(phone);
    }

    public boolean addMessenger(Messenger messenger) {
        return messengers.add(messenger);
    }

    public boolean removeMessenger(Messenger messenger) {
        return messengers.remove(messenger);
    }

    /**
     * Подписывается на обновления от публикатора.
     *
     * @param publisher публикатор
     */
    public void subscribe(Account publisher) {
        publisher.subscribers.add(this);
        subscriptions.add(publisher);
    }

    /**
     * Полностью отписывается от переданного в качестве аргумента аккаунта.
     *
     * @param publisher аккаунт, от которого нужно отписаться.
     */
    public void unsubscribe(Account publisher) {
        subscriptions.remove(publisher);
        publisher.subscribers.remove(this);
    }

    /**
     * Отправляет запрос в друзья.
     * Пока запрос не принят, текущий аккаунт будет находиться в подписчиках у запрашиваемого.
     *
     * @param friend запрашиваемый аккаунт.
     */
    public void sendFriendshipRequest(Account friend) {
        myFriendsPending.add(friend);
        friend.theirFriendsPending.add(this);
        subscribe(friend);
    }

    /**
     * Принимает запрос в друзья.
     * Если запрос от другого аккаунта не был послан, то метод не совершает никаких действий.
     *
     * @param pendingFriend аккаунт, от которого принимается запрос в друзья.
     */
    public boolean acceptFriendRequest(Account pendingFriend) {
        if (this.theirFriendsPending.contains(pendingFriend)) {
            theirFriendsPending.remove(pendingFriend);
            pendingFriend.myFriendsPending.remove(this);
            friends.add(pendingFriend);
            pendingFriend.friends.add(this);
            unsubscribe(pendingFriend);
            pendingFriend.unsubscribe(this);
            return true;
        } else {
            return false;
        }
    }

    protected boolean addFriend(Account friend) {
        return friends.add(friend);
    }

    /**
     * Запрос о дружбе отклоняется.
     * Аккаунт, отправивший запрос, остаётся в подписчиках.
     * Никаких действий не совершается, если запрос не был послан
     *
     * @param friend аккаунт, отправивший запрос
     */
    public void declineFriendshipRequest(Account friend) {
        if (theirFriendsPending.contains(friend)) {
            theirFriendsPending.remove(friend);
            friend.myFriendsPending.remove(this);
            friend.subscribe(this);
        }
    }

    /**
     * Удаляет аккаунт из друзей.
     * У удалённого аккаунта текущий остаётся в подписках.
     *
     * @param friend аккаунт, который удаляется из друзей
     */
    public boolean removeFriend(Account friend) {
        if (friends.contains(friend)) {
            friends.remove(friend);
            friend.friends.remove(this);
            friend.subscribe(this);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%s{id=%s, email=%s, %s %s %s}", getClass().getSimpleName(), id, email, firstName, lastName, middleName);
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object account) {
        if (account == null) {
            return false;
        }
        Account other = (Account) account;
        if (other.email.equals(this.email)) {
            return true;
        }
        return false;
    }

    public boolean addPermission(Permission permission) {
        return permissions.add(permission);
    }

    public boolean hasPermission(String permissionName) {
        return permissions.stream().anyMatch(x -> x.getName().equals(permissionName));
    }
}
