package com.getjavajob.training.web1610.shavrovo;

import java.util.HashSet;
import java.util.Set;

public class GroupBuilder {
    private int id;
    private String name;
    private Set<Account> admins;
    private Set<Account> members;


    public GroupBuilder(String name) {
        this.name = name;
    }

    public GroupBuilder id(int id) {
        this.id = id;
        return this;
    }

    public GroupBuilder members(Set<Account> members) {
        this.members = members;
        return this;
    }

    public GroupBuilder admins(Set<Account> admins) {
        this.admins = admins;
        return this;
    }

    public Group createGroup() {
        if (name == null) {
            return null;
        }
        if (members == null) {
            members = new HashSet<>();
        }
        if (admins == null) {
            admins = new HashSet<>();
        }
        return new Group(id, name, admins, members);
    }
}