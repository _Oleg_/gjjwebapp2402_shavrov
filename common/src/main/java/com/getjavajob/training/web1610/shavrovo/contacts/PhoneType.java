package com.getjavajob.training.web1610.shavrovo.contacts;

/**
 * Created by saul on 14.04.2017.
 */
public enum PhoneType implements ContactType {
    PERSONAL, HOME, WORK, ADDITIONAL;

    @Override
    public String contactType() {
        return this.name();
    }

    public static ContactType findEnum(String contactType) {
        for (PhoneType type : PhoneType.values()) {
            if (contactType.equals(type.name())) {
                return type;
            }
        }
        return null;
    }
}

