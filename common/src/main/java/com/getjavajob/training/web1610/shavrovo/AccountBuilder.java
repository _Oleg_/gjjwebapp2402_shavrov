package com.getjavajob.training.web1610.shavrovo;

import com.getjavajob.training.web1610.shavrovo.contacts.Messenger;
import com.getjavajob.training.web1610.shavrovo.contacts.Phone;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

public class AccountBuilder {
    private int id;
    private Timestamp regTimestamp;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String homeAddress;
    private String workAddress;
    private String avatarPath;

    private Set<Phone> phones;
    private Set<Messenger> messengers;
    private Set<Account> friends;
    private Set<Account> myFriendsPending;
    private Set<Account> theirFriendsPending;
    private Set<Account> subscribers;
    private Set<Account> subscriptions;
    private Set<Group> groups;
    private Set<Permission> permissions;

    public AccountBuilder() {
        this.id = -0xAB;
    }

    public AccountBuilder id(int id) {
        this.id = id;
        return this;
    }

    public AccountBuilder regTimestamp(Timestamp regTimestamp) {
        this.regTimestamp = regTimestamp;
        return this;
    }

    public AccountBuilder email(String email) {
        this.email = email;
        return this;
    }

    public AccountBuilder password(String password) {
        this.password = password;
        return this;
    }

    public AccountBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public AccountBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public AccountBuilder middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public AccountBuilder homeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
        return this;
    }

    public AccountBuilder workAddress(String workAddress) {
        this.workAddress = workAddress;
        return this;
    }

    public AccountBuilder avatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
        return this;
    }

    public AccountBuilder phones(Set<Phone> phones) {
        this.phones = phones;
        return this;
    }

    public AccountBuilder messengers(Set<Messenger> messengers) {
        this.messengers = messengers;
        return this;
    }

    public AccountBuilder friends(Set<Account> friends) {
        this.friends = friends;
        return this;
    }

    public AccountBuilder myFriendsPending(Set<Account> myFriendsPending) {
        this.myFriendsPending = myFriendsPending;
        return this;
    }

    public AccountBuilder theirFriendsPending(Set<Account> theirFriendsPending) {
        this.theirFriendsPending = theirFriendsPending;
        return this;
    }

    public AccountBuilder subscribers(Set<Account> subscribers) {
        this.subscribers = subscribers;
        return this;
    }

    public AccountBuilder subscriptions(Set<Account> subscriptions) {
        this.subscriptions = subscriptions;
        return this;
    }

    public AccountBuilder groups(Set<Group> groups) {
        this.groups = groups;
        return this;
    }

    public AccountBuilder permissions(Set<Permission> permissions) {
        this.permissions = permissions;
        return this;
    }

    public Account createAccount() {
        if (phones == null) {
            phones = new HashSet<>();
        }
        if (messengers == null) {
            messengers = new HashSet<>();
        }
        if (friends == null) {
            friends = new HashSet<>();
        }
        if (myFriendsPending == null) {
            myFriendsPending = new HashSet<>();
        }
        if (theirFriendsPending == null) {
            theirFriendsPending = new HashSet<>();
        }
        if (subscribers == null) {
            subscribers = new HashSet<>();
        }
        if (subscriptions == null) {
            subscriptions = new HashSet<>();
        }
        if (groups == null) {
            groups = new HashSet<>();
        }

        if (regTimestamp == null) {
            regTimestamp = new Timestamp(System.currentTimeMillis());
        }
        if (permissions == null) {
            permissions = new HashSet<>();
        }
        return new Account(
                id,
                regTimestamp,
                email,
                password,
                firstName,
                lastName,
                middleName,
                homeAddress,
                workAddress,
                avatarPath,

                phones,
                messengers,

                friends,
                myFriendsPending,
                theirFriendsPending,
                subscribers,
                subscriptions,
                groups,
                permissions
        );
    }
}