package com.getjavajob.training.web1610.shavrovo;

import lombok.*;

/**
 * Created by saul on 5/13/2017.
 */

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class Permission {
    @NonNull
    private int id;
    private String name;
    private String description;
}
