package com.getjavajob.training.web1610.shavrovo.contacts;

/**
 * Created by saul on 13.04.2017.
 */
public abstract class Contact {
    private int id;
    private String login;
    private ContactType contactType;

    public Contact(String login, ContactType contactType) {
        this.login = login;
        this.contactType = contactType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    private ContactType defineContactType(String contactType) {
        for (PhoneType phoneType : PhoneType.values()) {
            if (contactType.equals(phoneType.name())) {
                return phoneType;
            }
        }
        for (MessengerType messengerType : MessengerType.values()) {
            if (contactType.equals(messengerType.name())) {
                return messengerType;
            }
        }
        return null;
    }

}
