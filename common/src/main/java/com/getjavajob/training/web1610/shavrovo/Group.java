package com.getjavajob.training.web1610.shavrovo;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by saul on 12.03.2017.
 */
public class Group {
    private int id;
    private String name;
    private Set<Account> admins;
    private Set<Account> members;

    public enum MemberStatus {
        ADMIN, MEMBER
    }

    Group(int id, String name, Set<Account> admins, Set<Account> members) {
        this.id = id;
        this.name = name;
        this.admins = admins;
        this.members = members;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Account> getAdmins() {
        return admins;
    }

    public void setAdmins(Set<Account> admins) {
        this.admins = admins;
    }

    public void assignAdmin(Account admin) {
        if (members.contains(admin)) {
            admins.add(admin);
        }
    }

    public void removeFromAdmins(Account admin) {
        if (members.contains(admin)) {
            admins.remove(admin);
        }
    }

    public Set<Account> getMembers() {
        return members;
    }

    public void setMembers(Set<Account> members) {
        this.members = members;
    }

    public void addMember(Account member) {
        members.add(member);
    }

    public void removeMember(Account member) {
        members.remove(member);
        admins.remove(member);
    }
}
